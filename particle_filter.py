# -*- coding: utf-8 -*-
"""
Created on Tue Jul 19 20:19:33 2016

@author: phil
"""

from math import *
from matrix import *
import random
import turtle
import copy
import numpy as np
import pdb
from ai4roboticsfp_volatile.hexbug import hexbug
from ai4roboticsfp_volatile.hexbug_world import hexbug_world
# from ai4roboticsfp_volatile.hexbug_utility import generate_hexbug
from ai4roboticsfp_volatile.hexbug_sim import run_hexbug

import matplotlib.pyplot as plt
# from dibujo_data import datacalculation

# import matplotlib.pyplot as plt

N = 1000  # number of particles.
plot_on = False
max_iters = 5

max_err = 600.
mod_var = .035
meas_var = .60
avg_num = 64
scale_fac = .997
a_val = .98
h_val = np.sqrt(1 - a_val ** 2.)

# hexbug parameters
mes_ps = 60.0
rp_min = 1800.0
rp_sec = (rp_min/60.0)
rp_measure = (rp_sec/mes_ps)

str_list = ['test01', 'test02', 'test03', 'test04', 'test05', 'test06', 'test07',
            'test08', 'test09', 'test10']

max_dist = 30.

mean_col_a_c = .945
mean_col_d_c = .502
mean_d_dist = 6.24
mean_u_dist = 7.45
mean_d_turn = .311
mean_u_turn = -.160

std_col_a_c = .185
std_col_d_c = .190
std_d_dist = 2.71
std_u_dist = 2.86
std_d_turn = .64
std_u_turn = .316


def limit_col_d_c(col_d_c):
    return col_d_c % 1.


def limit_col_a_c(col_a_c):
    return col_a_c % 2.


def clamp_dist(distance):
    if distance < 0:
        return 0.
    if distance > max_dist:
        return max_dist

    return distance


def calc_init_state(m_list):

    u_vec = vec_diff(m_list[0], m_list[1])
    v_vec = vec_diff(m_list[1], m_list[2])
    u_matrix = matrix([[u_vec[0]], [u_vec[1]]])
    v_matrix = matrix([[v_vec[0]], [v_vec[1]]])

    u_dot_v = u_matrix.transpose() * v_matrix
    den = mag(u_vec) * mag(v_vec)

    step_size = (mag(u_vec) + mag(v_vec)) / 2.  # get average step size.
    ratio = u_dot_v.value[0][0] / (den + 1E-9)

    if abs(ratio) > 1.:
        ratio = np.sign(ratio) * 1.

    theta = np.arccos(ratio)
    curr_alpha = atan2(v_vec[1], v_vec[0])
    return (theta, step_size, curr_alpha)


def get_measurements(f_name):
    """
        function runs through the data forwards and backwards, calculating state vectors
        (curr_pos, next_pos, velocity, heading).  state values with significant differences
        between the forward and backward calculations are indicative of a collision or bounce motion
        that requires further analysis.

        curr_*  represent current state values (velocity, position, heading)
        next_*  represent next state values (velocity, position, heading)  This is the solution
                that the neural network is being trained against.
    """
    data_array = []
    with open(f_name, 'r') as fh:
        for line in iter(fh):
            a = line.split(',')
            point = [float(a[0]), float(a[1])]
            data_array.append(point)
    fh.close()

    return data_array


def circ_gauss_prob(x, y, mean_x, mean_y, sigma=.5):
    fac = 1 / (2 * np.pi * sigma**2.)
    exp_fac = ((x - mean_x)**2. + (y - mean_y)**2.) / (2 * sigma**2)

    return fac * np.exp(-exp_fac)


def angle_trunc(a):
    """This maps all angles to a domain of [-pi, pi]"""
    while a < 0.0:
        a += pi * 2
    return ((a + pi) % (pi * 2)) - pi


def angle_mod(a):
    """This maps all angles to a domain of [-pi, pi]"""
    return a % (2 * pi)


def get_heading(hunter_position, target_position):
    """Returns the angle, in radians, between the target and hunter positions"""
    hunter_x, hunter_y = hunter_position
    target_x, target_y = target_position
    heading = atan2(target_y - hunter_y, target_x - hunter_x)
    heading = angle_trunc(heading)
    return heading


def distance_between(point1, point2):
    """Computes distance between point1 and point2. Points are (x, y) pairs."""
    x1, y1 = point1
    x2, y2 = point2
    return sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)


def mag(vec):
    return sqrt(vec[0]**2. + vec[1]**2.)


def vec_diff(vec0, vec1):
    return (vec1[0] - vec0[0], vec1[1] - vec0[1])


# particle filter simulator -- written around Kenyon Jones' hexbug particle.
class Particle_Sim(object):

    def __init__(self, part_obj, N=1000):
        # particle class.
        self.part_obj = part_obj
        # number of particles
        self.N = N

    def twiddle(self, params, max_distance, threshold=.001):
        # Define potential changes
        dp = [1.] * len(params)

        speed_const = None
        param_indices = range(6)
        (best_err, _) = run_sim(params, max_distance, speed_const)
        while sum([dp[idx] for idx in param_indices]) > threshold:
            for idx in param_indices:
                params[idx] += dp[idx]
                try:
                    (err, _) = run_sim(params, max_distance, speed_const)
                except:
                    err = 1E6
                    # pdb.set_trace()
                if err < best_err:  # There was some improvement
                    best_err = err
                    dp[idx] *= 1.05
                else:  # There was no improvement
                    params[idx] -= 2*dp[idx]  # Go into the other direction
                    (err, _) = run_sim(params, max_distance, speed_const)

                    if err < best_err:  # There was an improvement
                        best_err = err
                        dp[idx] *= 1.05
                    else:  # There was no improvement
                        params[idx] += dp[idx]
                        # As there was no improvement, the step size in either
                        # direction, the step size might simply be too big.
                        dp[idx] *= 0.95

        (err, pos) = run_sim(params, max_distance, speed_const)
        params = [val / 1.5 for val in params]
        print params, err

        return params

    def run_sim(self, measurements, hbw, trained=False):
        # use first three measurements to get initial heading, turning, and speed.
        init_list = measurements[:3]
        (theta, step_size, curr_alpha) = calc_init_state(init_list)

        curr_pos = measurements[2]
        # generate particles.
        new_heading = curr_alpha + theta
        # print "New Heading = {}".format(new_heading)
        div_cnt = 0
        div_list = []

        # delta_y = step_size * sin(new_heading)
        # delta_x = step_size * cos(new_heading)

        # this forms the initial estimate of the particles
        # xy_estimate = (curr_pos[0] + delta_x, curr_pos[1] + delta_y)

        particles = []
        # noise_val = mod_var * step_size
#        d_noise_val = mod_var * step_size
#        t_noise_val = mod_var
#        c_noise_val = mod_var
        new_list = measurements[3:]
        for ii in range(self.N):
            # 2.1, 4.3, 0.5, 2*pi / 34.0, 1.5
            # 4.97014, 7.69929, 1.50000, 0.18480, 1.05440
            if trained is False:
                x = np.random.normal(curr_pos[0], .1 * step_size)
                y = np.random.normal(curr_pos[1], .1 * step_size)
                heading = np.random.normal(new_heading, .90)
                u_turning = angle_mod(np.random.normal(theta, np.pi))
                d_turning = angle_mod(np.random.normal(theta, np.pi))
                u_distance = clamp_dist(np.random.normal(step_size, .5 * step_size))
                d_distance = clamp_dist(np.random.normal(step_size, .5 * step_size))
                # u_distance = np.random.normal(.452, .5 * step_size)
                # d_distance = np.random.normal(7.2, .5 * step_size)
                col_d_c = limit_col_d_c(abs(np.random.normal(.5, 1.0)))
                col_a_c = limit_col_a_c(abs(np.random.normal(1.0, 1.5)))
            else:
                x = np.random.normal(curr_pos[0], .03 * step_size)
                y = np.random.normal(curr_pos[1], .03 * step_size)
                heading = np.random.normal(new_heading, .10)
                u_turning = angle_mod(np.random.normal(mean_u_turn, std_u_turn))
                d_turning = angle_mod(np.random.normal(mean_d_turn, std_d_turn))
                u_distance = clamp_dist(np.random.normal(mean_u_dist, std_u_dist))
                d_distance = clamp_dist(np.random.normal(mean_d_dist, std_d_dist))
                # u_distance = np.random.normal(.452, .5 * step_size)
                # d_distance = np.random.normal(7.2, .5 * step_size)
                col_d_c = limit_col_d_c(abs(np.random.normal(mean_col_d_c, std_col_d_c)))
                col_a_c = limit_col_a_c(abs(np.random.normal(mean_col_a_c, std_col_a_c)))

            r_p = hexbug(x=x, y=y,
                         heading=heading,
                         u_turning=u_turning,
                         d_turning=d_turning,
                         u_distance=u_distance,
                         d_distance=d_distance,
                         col_d_c=col_d_c,
                         col_a_c=col_a_c,
                         m_o=angle_trunc(pi + (pi/2)))

            r_p.set_hbw(hbw)
            # Do something if you want

            # start with initially high noise values.
#            r_p.set_noise(t_noise_val, t_noise_val,
#                          d_noise_val, d_noise_val,
#                          c_noise_val, c_noise_val)

            r_p.set_noise(0., 0.,
                          0., 0.,
                          0., 0.)

            # r_p.set_noise(0.005, 0.005, 0.00)
            particles.append(r_p)

        old_pos = measurements[2]
        particles = self.project_particles(particles)
        # now have initial set of particles
        # start prediction, measurement, noise adjustment loop.

        xy_list = []
        xy_list.append(self.gen_est(particles))
        cnt_last_div = 0
        for meas in new_list:
            # project particles
            curr_pos = self.gen_est(particles)
            square_err = distance_between(curr_pos, meas) ** 2.
            # print square_err
#            if square_err > 100000:
#                pdb.set_trace()
            m_var = meas_var
            flag = False
            if square_err > max_err:
                flag = True
                cnt = 0
                while square_err > (max_err / 4.):
                    # the was likely a collision -- particles likely had poor col_a_c and col_d_c
                    # values.
                    m_var = meas_var
                    # while square_err > 100:
                    particles = self.reset_parts(particles, old_pos, hbw)
                    particles = self.project_particles(particles)
                    update_probs = self.update_weights(meas, particles, m_var)
                    # problem with resampling.
                    particles = self.resamp_wheel(particles, update_probs)
                    new_pos = self.gen_est(particles)

                    square_err = distance_between(new_pos, meas) ** 2
                    print "new_pos = {}, meas = {}, new square error = {}".format(new_pos, meas,
                                                                                  square_err)

                    cnt += 1
                    if cnt == 5:
                        # degenerative case -- bail out and reset particles.
                        particles = self.reset_parts(particles, meas, hbw)
                        break

            if flag:
                div_cnt += 1
                div_list.append(cnt_last_div)
                # particles = self.reset_parts(particles, meas, hbw)
                cnt_last_div = 0

            cnt_last_div += 1
            # print square_err
            # pdb.set_trace()
            old_pos = self.gen_est(particles)
            update_probs = self.update_weights(meas, particles, m_var)
            # problem with resampling.
            new_particles = self.resamp_wheel(particles, update_probs)
            # now update parameters
            particles = self.update_params(new_particles)

            # print "before"
            particles = self.project_particles(new_particles)
            # print "after"
            # particles = self.update_m_var(particles, scale_fac)
            new_pos = self.gen_est(particles)

            square_err = distance_between(old_pos, new_pos) ** 2.
#            if square_err is np.nan:
#                pdb.set_trace()

            xy_list.append(new_pos)

        print div_cnt, div_list
        return (xy_list, self.ret_param_stats(particles))
#        for meas in measurements:
#            init_list.append(meas)
#            if init_list

    def reset_parts(self, particles, meas, hbw, scale_fac=10.):
        new_parts = []
        stats = self.ret_param_stats(particles)
        # scale2 = 10. # scale_fac / 5.
        for ii, particle in enumerate(particles):
            x = np.random.normal(meas[0], stats['std_x'])
            y = np.random.normal(meas[1], stats['std_y'])
            heading = np.random.normal(particle.heading, .4)  # stats['std_head'])
            temp = np.random.normal(particle.u_turning, 2)  # scale2 * stats['std_u_turn'])
            u_turning = angle_mod(temp)
            temp = np.random.normal(particle.d_turning, 2)  # scale2 * stats['std_d_turn'])
            d_turning = angle_mod(temp)
            u_distance = clamp_dist(np.random.normal(particle.u_distance, 3))
            # scale2 * stats['std_u_dist'])
            d_distance = clamp_dist(np.random.normal(particle.d_distance, 3))
            # scale2 * stats['std_d_dist'])

            col_d_c = limit_col_d_c(abs(np.random.normal(particle.col_d_c, .5)))
            # scale_fac * stats['std_col_dc']))
            col_a_c = limit_col_a_c(abs(np.random.normal(particle.col_a_c, .5)))
            # scale_fac * stats['std_col_ac']))
            # col_d_c = .6 # abs(np.random.normal(1., scale_fac * stats['std_col_dc']))
            # col_a_c = .4 # abs(np.random.normal(1., scale_fac * stats['std_col_ac']))

            r_p = hexbug(x=x, y=y,
                         heading=heading,
                         u_turning=u_turning,
                         d_turning=d_turning,
                         u_distance=u_distance,
                         d_distance=d_distance,
                         col_d_c=col_d_c,
                         col_a_c=col_a_c,
                         m_o=angle_trunc(pi + (pi / 2)))

            r_p.set_hbw(hbw)

            new_parts.append(r_p)

        return new_parts

    def ret_param_stats(self, particles):
        """
            Helper function that returns the mean of all of the particle parameters.
        """

        u_dist_list = []
        d_dist_list = []
        u_turn_list = []
        d_turn_list = []
        col_d_c_list = []
        col_a_c_list = []
        x_list = []
        y_list = []
        head_list = []
        for particle in particles:
            x_list.append(particle.x)
            y_list.append(particle.y)
            u_dist_list.append(particle.u_distance)
            d_dist_list.append(particle.d_distance)
            u_turn_list.append(particle.u_turning)
            d_turn_list.append(particle.d_turning)
            col_d_c_list.append(particle.col_d_c)
            col_a_c_list.append(particle.col_a_c)
            head_list.append(particle.heading)

        m_u_dist = np.mean(u_dist_list)
        std_u_dist = np.sqrt((h_val ** 2) * np.var(u_dist_list))

        m_d_dist = np.mean(d_dist_list)
        std_d_dist = np.sqrt((h_val ** 2) * np.var(d_dist_list))

        m_u_turn = np.mean(u_turn_list)
        std_u_turn = np.sqrt((h_val ** 2) * np.var(u_turn_list))

        m_d_turn = np.mean(d_turn_list)
        std_d_turn = np.sqrt((h_val ** 2) * np.var(d_turn_list))

        m_coldc = np.mean(col_d_c_list)
        std_coldc = np.sqrt((h_val ** 2) * np.var(col_d_c_list))

        m_colac = np.mean(col_a_c_list)
        std_colac = np.sqrt((h_val ** 2) * np.var(col_a_c_list))

        std_x = np.std(x_list)
        std_y = np.std(y_list)
        std_head = np.std(head_list)

        ret_dict = {}

        ret_dict['u_distance'] = m_u_dist
        ret_dict['d_distance'] = m_d_dist
        ret_dict['u_turning'] = m_u_turn
        ret_dict['d_turning'] = m_d_turn
        ret_dict['col_d_c'] = m_coldc
        ret_dict['col_a_c'] = m_colac
        ret_dict['std_u_dist'] = std_u_dist + 1E-9
        ret_dict['std_d_dist'] = std_d_dist + 1E-9
        ret_dict['std_u_turn'] = std_u_turn + 1E-9
        ret_dict['std_d_turn'] = std_d_turn + 1E-9
        ret_dict['std_col_dc'] = std_coldc + 1E-9
        ret_dict['std_col_ac'] = std_colac + 1E-9
        ret_dict['std_x'] = std_x + 1E-9
        ret_dict['std_y'] = std_y + 1E-9
        ret_dict['std_head'] = std_head + 1E-9

        return ret_dict

    def update_params(self, particles):
        # compute mean and variances of all parameters.
#        u_dist_list = []
#        d_dist_list = []
#        u_turn_list = []
#        d_turn_list = []
#        col_d_c_list = []
#        col_a_c_list = []

        u_dist_list = [particle.u_distance for particle in particles]
        d_dist_list = [particle.d_distance for particle in particles]
        u_turn_list = [particle.u_turning for particle in particles]
        d_turn_list = [particle.d_turning for particle in particles]
        col_d_c_list = [particle.col_d_c for particle in particles]
        col_a_c_list = [particle.col_a_c for particle in particles]
#        for particle in particles:
#            u_dist_list.append(particle.u_distance)


        m_u_dist = np.mean(u_dist_list)
        std_u_dist = np.sqrt((h_val ** 2) * np.var(u_dist_list)) + 1E-9

        m_d_dist = np.mean(d_dist_list)
        std_d_dist = np.sqrt((h_val ** 2) * np.var(d_dist_list)) + 1E-9

        m_u_turn = np.mean(u_turn_list)
        std_u_turn = np.sqrt((h_val ** 2) * np.var(u_turn_list)) + 1E-9

        m_d_turn = np.mean(d_turn_list)
        std_d_turn = np.sqrt((h_val ** 2) * np.var(d_turn_list)) + 1E-9

        m_coldc = np.mean(col_d_c_list)
        std_coldc = np.sqrt((h_val ** 2) * np.var(col_d_c_list)) + 1E-9

        m_colac = np.mean(col_a_c_list)
        std_colac = np.sqrt((h_val ** 2) * np.var(col_a_c_list)) + 1E-9

        # print "std_colac = {}".format(std_colac)
        for particle in particles:
            new_ud = a_val * particle.u_distance + (1 - a_val) * m_u_dist
            particle.u_distance = clamp_dist(np.random.normal(new_ud, std_u_dist))
            new_dd = a_val * particle.d_distance + (1 - a_val) * m_d_dist
            particle.d_distance = clamp_dist(np.random.normal(new_dd, std_d_dist))

            new_ut = a_val * particle.u_turning + (1 - a_val) * m_u_turn
            particle.u_turning = angle_trunc(np.random.normal(new_ut, std_u_turn))
            new_dt = a_val * particle.d_turning + (1 - a_val) * m_d_turn
            particle.d_turning = angle_trunc(np.random.normal(new_dt, std_d_turn))

            new_coldc = a_val * particle.col_d_c + (1 - a_val) * m_coldc
            particle.col_d_c = np.random.normal(new_coldc, std_coldc) % 1.
            new_colac = a_val * particle.col_a_c + (1 - a_val) * m_colac
            particle.col_a_c = np.random.normal(new_colac, std_colac) % 2.

        return particles

    def comp_avg_speed(self, particles):
        distance = 0
        for particle in particles:
            distance += particle.d_distance
            distance += particle.u_distance

        return distance / len(particles) / 2.

    def update_m_var(self, particles, scale=.99):
        """
            Helper function that updates the model variances of all the surviving particles
        """
        for particle in particles:
            t_noise = particle.turning_noise
            d_noise = particle.distance_noise
            m_noise = particle.measurement_noise
            particle.set_noise(t_noise * scale, d_noise * scale, m_noise * scale)

        return particles

    def update_weights(self, measurement, particles, meas_var=.25):
        step_size = self.comp_avg_speed(particles)
        update_probs = []
        meas_x = measurement[0]
        meas_y = measurement[1]
        for particle in particles:
            # compute circular Gaussian probability .
            # measurement is taken as the mean
            x = particle.x
            y = particle.y

            # this is the multiplication step
            val = circ_gauss_prob(x, y, meas_x, meas_y, meas_var * step_size)
            if np.isnan(val):
                val = 0
            update_probs.append(val)
        # normalize all probabilities.
        norm_fac = np.sum(update_probs)
        update_probs = [value / norm_fac for value in update_probs]

        return update_probs

    def resamp_wheel(self, particles, weights):
        num_parts = len(particles)
        new_particles = []
        index = int(random.random() * num_parts)
        # print index
        beta = 0.0
        mw = np.max(weights)
        for i in range(num_parts):
            beta += random.random() * 2.0 * mw
            # print "beta =", beta
            while beta > weights[index]:
                beta -= weights[index]
                index = (index + 1) % num_parts

            # print "\tbeta= %f, index = %d, weight = %f" % (beta, index, weights[index])
            new_particles.append(copy.copy(particles[index]))

        return new_particles

    def project_particles(self, particles):
        # parts = particles[:]  # copy.deepcopy(particles)
        proj_particles = []
        # for particle in parts:
        for temp in particles:
            particle = copy.copy(temp)
            particle.r_move(rp_measure)  # particle.turning, particle.distance)
            proj_particles.append(particle)

        return proj_particles

    def gen_est(self, particles):

        val_x = [val.x for val in particles]
        val_y = [val.y for val in particles]

        x = np.mean(val_x)
        y = np.mean(val_y)

        return (x, y)

    def estimate_next_pos(measurement, OTHER=None, plot_on=False):
        """Estimate the next (x, y) position of the wandering Traxbot
        based on noisy (x, y) measurements."""
        # pdb.set_trace()

        if OTHER is None:
            OTHER = [measurement]
            return measurement, OTHER
        else:
            if len(OTHER) < avg_num:
                OTHER.append(measurement)
                return (measurement, OTHER)
            # take 4 sets of measurments -- allows a more accurate estimate of turning and distance
            elif len(OTHER) == avg_num:
                # compute next step size and turning angle.
                theta = 0
                step_size = 0
                for j in range(avg_num / 4):
                    other_var = OTHER[j*4:j*4+4]
                    u_vec = vec_diff(other_var[-3], other_var[-2])
                    v_vec = vec_diff(other_var[-2], other_var[-1])
                    u_matrix = matrix([[u_vec[0]], [u_vec[1]]])
                    v_matrix = matrix([[v_vec[0]], [v_vec[1]]])

                    u_dot_v = u_matrix.transpose() * v_matrix
                    den = mag(u_vec) * mag(v_vec)

                    step_size += (mag(u_vec) + mag(v_vec)) / 2.  # get average step size.
                    ratio = u_dot_v.value[0][0] / den

                    if abs(ratio) > 1.:
                        ratio = np.sign(ratio) * 1.

                    theta += np.arccos(ratio)
                    # print theta, step_size

                theta /= avg_num / 4.
                step_size /= avg_num / 4.

                curr_alpha = atan2(v_vec[1], v_vec[0])

                new_heading = curr_alpha + theta
                # print "New Heading = {}".format(new_heading)

                delta_y = step_size * sin(new_heading)
                delta_x = step_size * cos(new_heading)

                # this forms the initial estimate of the particles
                xy_estimate = (measurement[0] + delta_x, measurement[1] + delta_y)

                particles = []
                noise_val = mod_var * step_size
                for ii in range(N):
                    # 2.1, 4.3, 0.5, 2*pi / 34.0, 1.5
                    # 4.97014, 7.69929, 1.50000, 0.18480, 1.05440
                    r_p = robot(np.random.normal(measurement[0], .10 * step_size),
                                np.random.normal(measurement[1], .10 * step_size),
                                heading=np.random.normal(new_heading, .15),
                                turning=np.random.normal(theta, .15),
                                distance=np.random.normal(step_size, .05 * step_size))

                    r_p.set_noise(noise_val, noise_val, 0.00)
                    # r_p.set_noise(0.005, 0.005, 0.00)
                    particles.append(r_p)

                particles = project_particles(particles)
                return (measurement, particles)

            else:
                particles = OTHER

                # update probabilities and resample.
                (x_curr, y_curr) = gen_est(particles)
                update_probs = update_weights(measurement, particles)

                # problem with resampling.
                new_particles = resamp_wheel(particles, update_probs)
                # print np.max(update_probs)
                # calculate current center of max of particles -- this is estimate
                # p_speed = np.mean([particle.distance for particle in particles])
                particles = project_particles(new_particles)

                (x, y) = gen_est(particles)
                xy_estimate = (x, y)

                if np.any(np.isnan(xy_estimate)):
                    pdb.set_trace()

                # You must return xy_estimate (x, y), and OTHER (even if it is None)
                # in this order for grading purposes.
                return xy_estimate, particles

        if OTHER is None:
            # create particles -- assuming map is 10 x 10.  Evenly distribute the particles
            # throughout the map.
            particles = []

            meas_idx = 0
            # pred_probs = [1./N] * N
            # received measurement perform weight calculation (conditional probability)
            # $p(z_{k}|x_{k})$
            # $p(x_{k} | z_{1:k-1})$ generated from state prediction step -- hasn't occurred yet.
            update_probs = update_weights(measurement, particles)

            particles = resamp_wheel(particles, update_probs)
            # now inject random headings and speed to new particles
            # particles now how heading and speed
            OTHER = (particles, meas_idx)
            return measurement, OTHER

#    def run_sim(self, measurements):
#        # loop for running measurements
#        for measurement in measurements:


def generate_hexbug(x, y, o):
    x = x
    y = y
    o = pi / 10  # np.random.normal(o, (2*pi/10))%(2*pi)
    u_turning = .15  # np.random.normal(0.0, (2*pi/10))
    d_turning = .25  # np.random.normal(0.0, (2*pi/10))
    u_distance = 8.0  # abs(random.gauss(0.0, 10.0))+5.0
    d_distance = 7.0  # abs(random.gauss(0.0, 10.0))+5.0
    col_d_c = 1.0  # abs(random.gauss(0.5, 0.5))
    col_a_c = .4  # abs(random.gauss(1.0, 1.0))
    m_o = .4  # random.gauss(0.0, pi)
    h = hexbug(x, y, o, u_turning, d_turning, u_distance, d_distance, col_d_c, col_a_c, m_o)

    return h


def gen_ideal_hexbug(init_list, params_est, hbw):

    u_turning = params_est['u_turning']
    d_turning = params_est['d_turning']
    u_distance = params_est['u_distance']
    d_distance = params_est['d_distance']
    col_d_c = params_est['col_d_c']
    col_a_c = params_est['col_a_c']

    (theta, step_size, curr_alpha) = calc_init_state(init_list)

    curr_pos = init_list[2]
    new_heading = curr_alpha + theta

    r_p = hexbug(x=curr_pos[0], y=curr_pos[1],
                 heading=new_heading,
                 u_turning=u_turning,
                 d_turning=d_turning,
                 u_distance=u_distance,
                 d_distance=d_distance,
                 col_d_c=col_d_c,
                 col_a_c=col_a_c,
                 m_o=angle_trunc(pi + (pi/2)))

    r_p.set_hbw(hbw)

    return r_p


def gen_stats(suffix='_train'):
    import cPickle as pickle
    from collections import OrderedDict

    dict_list = []

    for str_val in str_list:
        dict_list.append(pickle.load(open('{}{}.p'.format(str_val, suffix), 'rb')))

    col_a_c = []
    col_d_c = []
    d_distance = []
    u_distance = []
    u_turning = []
    d_turning = []

    for dict_val in dict_list:
        col_a_c.append(dict_val['col_a_c'])
        col_d_c.append(dict_val['col_d_c'])
        d_distance.append(dict_val['d_distance'])
        u_distance.append(dict_val['u_distance'])
        d_turning.append(dict_val['d_turning'])
        u_turning.append(dict_val['u_turning'])

    mean_d_dist = np.mean(d_distance)
    mean_u_dist = np.mean(u_distance)
    mean_col_a_c = np.mean(col_a_c)
    mean_col_d_c = np.mean(col_d_c)
    mean_u_turn = np.mean(u_turning)
    mean_d_turn = np.mean(d_turning)

    std_d_dist = np.std(d_distance)
    std_u_dist = np.std(u_distance)
    std_col_a_c = np.std(col_a_c)
    std_col_d_c = np.std(col_d_c)
    std_u_turn = np.std(u_turning)
    std_d_turn = np.std(d_turning)

    dict_val = OrderedDict()

    dict_val['mean_d_dist'] = mean_d_dist
    dict_val['mean_u_dist'] = mean_u_dist
    dict_val['mean_col_a_c'] = mean_col_a_c
    dict_val['mean_col_d_c'] = mean_col_d_c
    dict_val['mean_u_turn'] = mean_u_turn
    dict_val['mean_d_turn'] = mean_d_turn

    dict_val['std_d_dist'] = std_d_dist
    dict_val['std_u_dist'] = std_u_dist
    dict_val['std_col_a_c'] = std_col_a_c
    dict_val['std_col_d_c'] = std_col_d_c
    dict_val['std_u_turn'] = std_u_turn
    dict_val['std_d_turn'] = std_d_turn

    print dict_val

    pickle.dump(dict_val, open('final_results.p', 'wb'))


def predict(file_name='./dibujo_data/test01.txt'):
    import time

    boundaries = []
    boundaries.append(['x', 'lt', 78])
    boundaries.append(['x', 'gt', 555])  # was 561
    boundaries.append(['y', 'lt', 40])  # was 37
    boundaries.append(['y', 'gt', 320])  # was 326
    boundaries.append(['circle', 331, 181, 42])  # was 331, 181, 37
    hbw = hexbug_world(boundaries)

    # print params
    num_particles = 400
    num_meas = 220
    num_preds = 60
    t1 = time.time()
    mt = get_measurements(file_name)
    mt = mt[-num_meas:]

    sim_obj = Particle_Sim(hexbug, N=num_particles)

    xy_out, params_est = sim_obj.run_sim(mt, hbw, trained=True)

    ideal_part = gen_ideal_hexbug(mt[-3:], params_est, hbw)
    pred_list = []
    for ii in range(num_preds):
        ideal_part.r_move(rp_measure)
        pred_list.append((ideal_part.x, ideal_part.y))

    # now generate particle based on params_est
    t2 = time.time()
    print "total time = {}".format(t2-t1)

    # print params
    print params_est
    print pred_list
    return pred_list


def predict_test():
    import time
    import cPickle as pickle


#    x_init = 200.0
#    y_init = 200.0
#    o_init = 0.0

    # time_s = 20.0

    boundaries = []
    boundaries.append(['x', 'lt', 78])
    boundaries.append(['x', 'gt', 555])  # was 561
    boundaries.append(['y', 'lt', 40])  # was 37
    boundaries.append(['y', 'gt', 320])  # was 326
    boundaries.append(['circle', 331, 181, 42])  # was 331, 181, 37
    hbw = hexbug_world(boundaries)
    # hb = generate_hexbug(x_init, y_init, o_init)
    # hb.set_hbw(hbw)
    # hbw.set_hb(hb)

    # hb_list.append(hb)
    # _, mt,  params = (run_hexbug(hb, time_s, mes_ps, rp_measure))

    # print params
    num_particles = 500
    num_meas = 220
    num_preds = 60

    # str_list = ['test01']

    for str_val in str_list:
        t1 = time.time()
        # generate state vectors
        mt = get_measurements('./dibujo_data/{}.txt'.format(str_val))
        mt = mt[-num_meas:]
        sim_obj = Particle_Sim(hexbug, N=num_particles)

        xy_out, params_est = sim_obj.run_sim(mt, hbw, trained=True)

        # now generate particle based on params_est
    #    xy_out = mt
    #    params_est = params
        pickle.dump(params_est, open('{}_predict.p'.format(str_val), 'wb'))
        t2 = time.time()
        print "total time = {}".format(t2-t1)

        # print params
        print params_est

    gen_stats('_predict')


def train():
    import time
    import cPickle as pickle

    x_init = 200.0
    y_init = 200.0
    o_init = 0.0

    # time_s = 20.0

    boundaries = []
    boundaries.append(['x', 'lt', 78])
    boundaries.append(['x', 'gt', 555])  # was 561
    boundaries.append(['y', 'lt', 40])  # was 37
    boundaries.append(['y', 'gt', 320])  # was 326
    boundaries.append(['circle', 331, 181, 42])  # was 331, 181, 37
    hbw = hexbug_world(boundaries)
    hb = generate_hexbug(x_init, y_init, o_init)
    hb.set_hbw(hbw)
    # hbw.set_hb(hb)

    # hb_list.append(hb)
    # _, mt,  params = (run_hexbug(hb, time_s, mes_ps, rp_measure))

    # print params
    num_particles = 6000

    # str_list = ['test01']

    for str_val in str_list:
        t1 = time.time()
        # generate state vectors
        mt = get_measurements('./dibujo_data/{}.txt'.format(str_val))
        # mt = mt[:400]
        sim_obj = Particle_Sim(hexbug, N=num_particles)

        xy_out, params_est = sim_obj.run_sim(mt, hbw)
    #    xy_out = mt
    #    params_est = params
        t2 = time.time()

        print "total time = {}".format(t2-t1)

        pickle.dump(params_est, open('{}_{}train.p'.format(str_val, num_particles), 'wb'))

        # print params
        print params_est

        hbpath_fig = plt.figure("Simulator Run - {}".format(str_val))
        plt.axis([0, 600, 0, 400])
        hbpath_ax = hbpath_fig.add_subplot(1, 1, 1)
        # circ = plt.Circle((boundaries[4][1],boundaries[4][2]),
        # radius=boundaries[4][3], color='g', fill=False)
        # hbpath_ax.add_patch(circ)
        # hbpath_ax.scatter(mt[0], mt[1])
        x_vals = [val[0] for val in mt]
        y_vals = [val[1] for val in mt]
        hbpath_ax.plot(x_vals, y_vals, 'ro-')  # color = 'red')
        xp_vals = [val[0] for val in xy_out]
        yp_vals = [val[1] for val in xy_out]
        hbpath_ax.plot(xp_vals, yp_vals, 'bx-')  # color = 'red')
        circ2 = plt.Circle((boundaries[4][1], boundaries[4][2]), radius=boundaries[4][3],
                           color='g', fill=False)
        hbpath_ax.add_patch(circ2)

        hbpath_ax.axvline(x=boundaries[0][2])
        hbpath_ax.axvline(x=boundaries[1][2])
        hbpath_ax.axhline(y=boundaries[2][2])
        hbpath_ax.axhline(y=boundaries[3][2])


if __name__ == "__main__":
    # gen_stats()
    # train()
    # predict()
    file_name = './inputs/test02.txt'

    num_preds = 60
    boundaries = []
    boundaries.append(['x', 'lt', 78])
    boundaries.append(['x', 'gt', 555])  # was 561
    boundaries.append(['y', 'lt', 40])  # was 37
    boundaries.append(['y', 'gt', 320])  # was 326
    boundaries.append(['circle', 331, 181, 42])  # was 331, 181, 37
    hbw = hexbug_world(boundaries)

    ideal_part = hexbug(x=506.0, y=191.0,
                        heading=-1.8139088695048957,
                        u_turning=-0.347847151585154,
                        d_turning=0.34424501161402055,
                        u_distance=8.5114807908012846,
                        d_distance=6.2983736133676196,
                        col_d_c=0.60462843042458836,
                        col_a_c=1.2637641418027696,
                        m_o=angle_trunc(pi + (pi/2)))

    ideal_part.set_hbw(hbw)

    pred_list = []
    for ii in range(num_preds):
        ideal_part.r_move(rp_measure)
        pred_list.append((ideal_part.x, ideal_part.y))



    # output(pred_list)
    draw_file(filename, "../finalproject/prediction.txt")

