import matplotlib.pyplot as plot
import pylab
from math import *
import pickle

#draw_file name pulls information from text file with just points  x,y  in the txt file
def draw_file1(name):
    #load data
    data_array=[]
    f = open(name)
    for line in iter(f):
        a=line.split(',')
        point=[float(a[0]),float(a[1])]
        data_array.append(point)
    f.close()
    #draw
    x=[]
    y=[]
    for i in range(len(data_array)):
        x.append(data_array[i][0])
        y.append(data_array[i][1])
    # change 2 lines below to change time to show on plot
    # for example x[-10:], y[-10:] will show last 10 points
    #x=x[-15:]
    #y=y[-15:]
    # print 'max x-',max(x)#561
    # print 'max y-', max(y)#326
    # print 'min x-', min(x)#78
    # print 'min y-', min(y)#37
    #linex = [min(x),min(x),max(x),max(x),min(x)]
    #liney = [min(y), max(y),max(y),min(y),min(y)]
    #add 2 on max and subtract 2 on min
    #linex = [76, 76, 563, 563, 76]
    #liney = [35, 328,328,35,35]
    #circx = floor(((max(x)+min(x))/2)+12)
    circx = 331
    circy = 181
    #circy = floor((max(y) + min(y)) / 2)
    circle = plot.Circle((circx, circy),37,  alpha=.2)
    ax=pylab.axes(aspect=1)
    #plot.plot(linex, liney)
    plot.plot(x, y, '-', color='yellow')
    ax.add_patch(circle)
    #s will change size of dot
    plot.scatter(x, y, s=2,color='red')
    #making dotted line to last point in data
    #plot.plot(x[-2:],y[-2:],'--',color='green')
    # putting asterix on last point in data
    #plot.plot(x[-1], y[-1], '*', color='green')
    plot.title(name+' plot')
    #plot.title(name + ' ' + str(len(x)) + ' seconds')
    plot.xlabel('x-axis')
    plot.ylabel('y-axis')
    plot.grid(True)
    #take out next three lines to just show on screen plot
    #fig1 = plot.gcf()
    #just_name=name.split('.')
    #fig1.savefig(just_name[0]+'_last_15_secs.png')
    #fig1.savefig(just_name[0] + '.png')
    #plot.clf()
    plot.show()

#draw_file name pulls information from text file with just points  x,y  in the txt file
def get_list(dictionary_item1,dictionary_item2,number,list):
    my_list=[]
    #my_list=list[dictionary_item]#[number]
    for i in range(len(list[dictionary_item1])):
        my_list.append(list[dictionary_item1][i][number])
    my_list.append(list[dictionary_item2][len(list[dictionary_item2])-1][number])
    return my_list

def draw_file_pickle(name):
    with open('test.p', 'rb') as handle:
        b = pickle.load(handle)
    # points that I will iterate through to mark path
    pointsx = get_list('curr_pos', 'next_pos', 0, b)
    pointsy = get_list('curr_pos', 'next_pos', 1, b)
    fig = plot.figure()
    ax = fig.add_subplot(1, 1, 1)
    # ax = plt.subplot(gs[0, :])
    ####this is for box and circle
    linex = [76, 76, 563, 563, 76]
    liney = [35, 328, 328, 35, 35]
    circx = floor(((max(linex) + min(linex)) / 2) + 12)
    circy = floor((max(liney) + min(liney)) / 2)
    circle = plot.Circle((circx, circy), 37, alpha=.2)
    ax = pylab.axes(aspect=1)
    plot.plot(linex, liney)
    ax.add_patch(circle)
    #####
    ax.grid(True)
    ax.plot(pointsx, pointsy, 'yo-', color='yellow')
    ax.scatter(pointsx, pointsy, s=2, color='red')
    #ax.plot(pointsx[-2:], pointsy[-2:], '--', color='green')
    #ax.plot(pointsx[-1], pointsy[-1], '*', color='green')
    plot.title('data file- test.p')
    # http://matplotlib.org/users/text_intro.html
    plot.ylabel('Trajectory')
    plot.show()

# name='test'
# for i in range(1,11):
#      if i<10:
#          file=name+'0'+str(i)+'.txt'
#      else:
#          file=name+str(i)+'.txt'
#      draw_file(file)
draw_file1('candle_circumference.txt')
#draw_file_pickle('test.p')

#draw_file1 - used for just points in txt file
#draw_file_pickle - used for data stored as dictionaries(uses pickle to pick up)