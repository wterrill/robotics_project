#Contents of folder alldata

##alldata.txt		
file containing data from test00.txt + training_data.txt

##join_data.py

used to join all the data sheets into alldata.txt

##alldatacalculation.py
used to read alltest00.txt files renamed training_data.txt as test11.txt to be included this produces the following:

* distance_orientation_alldata.txt this is all distances from point to point and the heading during that travel
* distance_points_heading_alldata.txt this is a list from point 1 to point2 distance and heading used during that move
* frequency_alldata.txt this is a frequency all distances from point to point in travel
* statdata_alldata.txt this is the mean interval and std of distances ~~just in case this can be used~~

##distance_orientation_alldata.xlsx

this file is used to get a frequency of distance and heading from distance_orientation_alldata.txt

##visualizealldata.py

* draw_file1 - used for just points in txt file(reads line by line to pick up x,y data)
* draw_file_pickle - used for data stored as dictionaries(uses pickle to pick up dictionary data)


## Where walls are located

To obtain collision data from the walls we will look at alldata.txt data is in the following format
    
450,122

458,109

first number means x and second y 

* max x-561  any x on 561 means the hexbug hit the _top wall_
* min x-78   any x on 78 means the hexbug hit the _bottom wall_
* max y-326  any y on 326 means the hexbug hit the _right wall_
* min y-37   any y on 37 means the hexbug hit the _left wall_

The points for the wall rectangle are the following: (76,328),(563,328),(563,35),(76,35)

## Where candle is located

By using the parametric equation for a circle in candle.py, the file candle_circumference.txt
is generated. When looking at the information in alldata.txt any point referenced in the candle
circumference file means the hexbug collided with the candle.

```
x = floor(cx + r * cos(a))
y = floor(cy + r * sin(a))
r     = radius
a     = angle
cx,cy = center 
```

## candle_circumference.txt

This file contains the circumference for the candle

## test_candle_data.py

This file is used to plot candle_circumference.txt and test the points within that file

## candle.py
This file is used to create the data within circumference.txt

## candle.png

This file shows the pixelated version of the candle

## candle_test.png

This file shows the pixelated version of the candle with a circle in the background to view veracity of data

