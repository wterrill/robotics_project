from math import *
import numpy as np
import scipy.stats as sci

def distance_between(point1, point2):
    """Computes distance between point1 and point2. Points are (x, y) pairs."""
    x1, y1 = point1
    x2, y2 = point2
    return sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

def angle_trunc(a):
    """This maps all angles to a domain of [-pi, pi]"""
    while a < 0.0:
        a += pi * 2
    return ((a + pi) % (pi * 2)) - pi

def get_heading(hunter_position, target_position):
    """Returns the angle, in radians, between the target and hunter positions"""
    hunter_x, hunter_y = hunter_position
    target_x, target_y = target_position
    heading = atan2(target_y - hunter_y, target_x - hunter_x)
    heading = angle_trunc(heading)
    return heading

def calculate_data(name):
    #load data
    data_array=[]
    f = open(name)
    for line in iter(f):
        a=line.split(',')
        point=[float(a[0]),float(a[1])]
        data_array.append(point)
    f.close()

    just_name = name.split('.')
    distance=[]
    f = open("distance_points_heading_"+just_name[0]+".txt", "w")
    f.write('   point # 1       point # 2         distance         heading from #1-#2 \n')
    f.write('======================================================================= \n')
    for i in range(len(data_array) - 1):
        value=[data_array[i], data_array[i + 1],distance_between(data_array[i], data_array[i + 1]),get_heading(data_array[i], data_array[i + 1])]
        distance.append(value)
        f.write(str("%s\n" % value))
    #distance array has points ,distance and heading between them
    f.close()
    a=np.array(distance)
    just_distance=a[:,[2]]
    #frequency of distance is obtained and stored
    dist_freq = sci.itemfreq(just_distance)
    np.savetxt('frequency_'+just_name[0]+'.txt', dist_freq[dist_freq[:, 1].argsort()[::-1]],fmt='%.1f',header='frequency of lengths found between all movements')
    #frequency and heading are stored
    distance_heading = a[:, [2,3]]
    np.savetxt('distance_orientation_' + just_name[0] + '.txt', distance_heading[distance_heading[:, 1].argsort()[::-1]], fmt='%.3f')
    #print "frequency-",xxx[xxx[:, 1].argsort()[::-1]]
    mean,var,std=sci.mvsdist(just_distance)
    f = open("statdata_" + just_name[0] + ".txt", "w")
    f.write(str("mean - %s\n" % mean.mean()))
    f.write("interval- {} \n".format(mean.interval(0.95)))
    f.write(str("std- %s\n" % mean.std()))
    f.close()

calculate_data('alldata.txt')
