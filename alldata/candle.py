import math
import pylab
import matplotlib.pyplot as plot

def candle_point(center,radius,angle):
    xpoint=center[0]+(radius*math.cos(angle))
    ypoint=center[1]+(radius*math.sin(angle))
    return math.floor(xpoint),math.floor(ypoint)

def draw_candle(circumference):
    # draw
    x = []
    y = []
    for i in range(len(circumference)):
        x.append(circumference[i][0])
        y.append(circumference[i][1])
    circx = 331
    circy = 181
    ax = pylab.axes(aspect=1)
    circle = plot.Circle((circx, circy), 37, alpha=.2)
    #ax.add_patch(circle)
    plot.scatter(x, y, s=2, color='red')
    plot.title('Candle Circumference')
    plot.show()

def save_candle_location(circumference):
    f = open("candle_circumference.txt", "w")
    for i in range(len(circumference) - 1):
        data=str(int(circumference[i][0]))+','+str(int(circumference[i][1]))
        f.write(data+'\n')
    f.close()

radius=37.0
#x = floor(((max(x)+min(x))/2)+12)
xcenter = math.floor(((561+78)/2)+12)
#y = floor((max(y) + min(y)) / 2)
ycenter = math.floor((326 + 37) / 2)
center=xcenter,ycenter
circumference=[]
for i in range(360):
    circumference.append(candle_point(center,radius,i))

draw_candle(circumference)
save_candle_location(circumference)