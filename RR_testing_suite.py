#!/usr/bin/python

import math
import random
import robot
import time
from StringIO import StringIO
import sys
import traceback

PI = math.pi

## STUDENT_ID: should be the student's GTID (for example, gburdell3)
## PART_TO_TEST: Set to 1, 2, 3, 4, or 5 depending on which part of the project is being tested

## The filename should have the form "<student_id>_<part_to_test>.py" (for example, gburdell3_1.py)

STUDENT_ID = 'cpryby3'
PART_TO_TEST = 3

### how close the guess/hunter needs to be to the target to call it a hit
### 0.02 is the ratio used by the autograder in the Udacity classroom
TOLERANCE_RATIO = 0.02


def main():
    parameters = []

    # Test Case 0 (from template code)
    params = {}
    params['test_case'] =         0
    params['target_x'] =          2.1
    params['target_y'] =          4.3
    params['target_heading'] =    0.5
    params['target_period'] =    34.0
    params['target_speed'] =      1.5
    params['hunter_x'] =        -10.0
    params['hunter_y'] =        -10.0
    params['hunter_heading'] =    0.0
    parameters.append(params)

    # Test Case 1 (from autograder)
    params = {}
    params['test_case'] =         1
    params['target_x'] =          5.3
    params['target_y'] =          6.2
    params['target_heading'] =    0.0
    params['target_period'] =    20.0
    params['target_speed'] =      2.3
    params['hunter_x'] =         -2.8
    params['hunter_y'] =        -10.2
    params['hunter_heading'] =    0.0
    parameters.append(params)

    # Test Case 2 (from autograder)
    params = {}
    params['test_case'] =         2
    params['target_x'] =         -3.9
    params['target_y'] =          9.5
    params['target_heading'] =   -2.09
    params['target_period'] =    31.0
    params['target_speed'] =      1.45
    params['hunter_x'] =         -5.7
    params['hunter_y'] =          3.2
    params['hunter_heading'] =    2.0
    parameters.append(params)

    # Test Case 3 (from autograder)
    params = {}
    params['test_case'] =         3
    params['target_x'] =        -10.2
    params['target_y'] =        -11.8
    params['target_heading'] =    0.79
    params['target_period'] =    13.0
    params['target_speed'] =      4.8
    params['hunter_x'] =          0.0
    params['hunter_y'] =          3.2
    params['hunter_heading'] =   -1.0
    parameters.append(params)


    for params in parameters:
        run_test_case(params)


def run_test_case(params):

    if PART_TO_TEST in (1,2):

        if PART_TO_TEST == 1:
            params['max_steps'] = 10
            params['noise_ratio'] = 0.0
        else:
            params['max_steps'] = 1000
            params['noise_ratio'] = 0.05

        exec('from {}_{} import estimate_next_pos'.format(STUDENT_ID, PART_TO_TEST))

        target = robot.robot(params['target_x'],
                             params['target_y'],
                             params['target_heading'],
                             2.0 * PI / params['target_period'],
                             params['target_speed'])
        target.set_noise(0.0,
                         0.0,
                         params['noise_ratio'] * params['target_speed'])

        start_time = time.clock()

        # swap stdout to suppress output from student code
        fake_stdout = StringIO()
        sys.stdout, fake_stdout = fake_stdout, sys.stdout

        try:
            localized, steps = simulate_without_hunter(estimate_next_pos, target, params)
        except Exception as e:
            exc_type, exc_val, exc_tb = sys.exc_info()
            exc_type_val = traceback.format_exception_only(exc_type, exc_val)[0]
            feedback = 'Your code raised an exception.\n{}'.format(exc_type_val)
            sys.stderr.write(feedback)

            localized, steps = False, params['max_steps']

        # swap stdout to allow output again
        sys.stdout, fake_stdout = fake_stdout, sys.stdout

        end_time = time.clock()
        tot_time = end_time - start_time

        extra = ', found in {} steps'.format(steps) if localized else ''

        print 'Test Case {}: {}{}, {} seconds'.format(params['test_case'], localized, extra, tot_time)



    elif PART_TO_TEST in (3,4,5):
        params['max_steps'] = 1000

        if PART_TO_TEST == 3:
            params['speed_ratio'] = 2.00
            params['noise_ratio'] = 0.05
        elif PART_TO_TEST == 4:
            params['speed_ratio'] = 0.99
            params['noise_ratio'] = 0.05
        else:
            params['speed_ratio'] = 0.99
            params['noise_ratio'] = 2.00

        exec('from {}_{} import next_move'.format(STUDENT_ID, PART_TO_TEST))

        target = robot.robot(params['target_x'],
                             params['target_y'],
                             params['target_heading'],
                             2.0 * PI / params['target_period'],
                             params['target_speed'])
        target.set_noise(0.0,
                         0.0,
                         params['noise_ratio'] * params['target_speed'])

        hunter = robot.robot(params['hunter_x'],
                             params['hunter_y'],
                             params['hunter_heading'])

        start_time = time.clock()

        # swap stdout to suppress output from student code
        fake_stdout = StringIO()
        sys.stdout, fake_stdout = fake_stdout, sys.stdout

        try:
            caught, steps = simulate_with_hunter(next_move, target, hunter, params)

        except Exception as e:
            exc_type, exc_val, exc_tb = sys.exc_info()
            exc_type_val = traceback.format_exception_only(exc_type, exc_val)[0]
            feedback = 'Your code raised an exception.\n{}'.format(exc_type_val)
            sys.stderr.write(feedback)

            caught, steps = False, params['max_steps']

        # swap stdout to allow output again
        sys.stdout, fake_stdout = fake_stdout, sys.stdout

        end_time = time.clock()
        tot_time = end_time - start_time

        extra = ', found in {} steps'.format(steps) if caught else ''

        print 'Test Case {}: {}{}, {} seconds'.format(params['test_case'], caught, extra, tot_time)


    else:
        raise ValueError('PART_TO_TEST should be an integer between 1 and 5, inclusive')


def simulate_without_hunter(estimate_next_pos, target, params):
    tolerance = TOLERANCE_RATIO * target.distance
    other_info = None
    steps = 0

    while steps < params['max_steps']:

        target_pos = (target.x, target.y)
        target_meas = target.sense()

        estimate, other_info = estimate_next_pos(target_meas, other_info)

        target.move_in_circle()
        target_pos = (target.x, target.y)

        separation = distance(estimate, target_pos)
        if separation < tolerance:
            return True, steps

        steps += 1

    return False, steps


def simulate_with_hunter(next_move, target, hunter, params):
    tolerance = TOLERANCE_RATIO * target.distance
    max_speed = params['speed_ratio'] * params['target_speed']
    other_info = None
    steps = 0

    while steps < params['max_steps']:

        hunter_pos = (hunter.x, hunter.y)
        target_pos = (target.x, target.y)

        separation = distance(hunter_pos, target_pos)
        if separation < tolerance:
            return True, steps

        target_meas = target.sense()
        turn, dist, other_info = next_move(hunter_pos, hunter.heading, target_meas, max_speed, other_info)

        dist = min(dist, max_speed)
        dist = max(dist, 0)
        turn = truncate_angle(turn)

        hunter.move(turn, dist)
        target.move_in_circle()

        steps += 1

    return False, steps


def distance(p, q):
    x1, y1 = p
    x2, y2 = q

    dx = x2 - x1
    dy = y2 - y1

    return math.sqrt(dx**2 + dy**2)


def truncate_angle(t):
    return ((t+PI)%(2*PI)) - PI


if __name__ == '__main__':
    main()