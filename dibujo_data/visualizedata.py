import matplotlib.pyplot as plot
import pylab
from math import *

def draw_file(name):
    #load data
    data_array=[]
    f = open(name)
    for line in iter(f):
        a=line.split(',')
        point=[float(a[0]),float(a[1])]
        data_array.append(point)
    f.close()
    #draw
    x=[]
    y=[]
    for i in range(len(data_array)):
        x.append(data_array[i][0])
        y.append(data_array[i][1])
    # change 2 lines below to change time to show on plot
    # for example x[-10:], y[-10:] will show last 10 points
    x=x[-30:]
    y=y[-30:]
    # print 'max x-',max(x)#561
    # print 'max y-', max(y)#326
    # print 'min x-', min(x)#78
    # print 'min y-', min(y)#37
    #linex = [min(x),min(x),max(x),max(x),min(x)]
    #liney = [min(y), max(y),max(y),min(y),min(y)]
    #add 2 on max and subtract 2 on min
    linex = [76, 76, 563, 563, 76]
    liney = [35, 328,328,35,35]
    circx = floor(((563+76)/2)+12)
    circy = floor((328 + 35) / 2)
    circle = plot.Circle((circx, circy),37,  alpha=.2)
    #circle=plot.Circle((332,177),35,alpha =.2)
    ax=pylab.axes(aspect=1)
    plot.plot(linex, liney)
    plot.plot(x, y, '-', color='yellow')
    ax.add_patch(circle)
    #s will change size of dot
    plot.scatter(x, y, s=2,color='red')
    #making dotted line to last point in data
    plot.plot(x[-2:],y[-2:],'--',color='green')
    # putting asterix on last point in data
    plot.plot(x[-1], y[-1], '*', color='green')
    plot.title(name+' last '+str(len(x)/30)+ ' second')
    #plot.title(name + ' ' + str(len(x)) + ' seconds')
    plot.xlabel('x-axis')
    plot.ylabel('y-axis')
    plot.grid(True)
    #take out next three lines to just show on screen plot
    fig1 = plot.gcf()
    just_name=name.split('.')
    fig1.savefig(just_name[0]+'_last_1_sec.png')
    #fig1.savefig(just_name[0] + '.png')
    plot.clf()
    #plot.show()


name='test'
for i in range(1,11):
     if i<10:
         file=name+'0'+str(i)+'.txt'
     else:
         file=name+str(i)+'.txt'
     draw_file(file)
#draw_file('test01.txt')