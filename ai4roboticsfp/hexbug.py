from math import *
import random
import hexbug_utility as hu
# helper function to map all angles onto [-pi, pi]

class hexbug:

    count = 0
    def __init__(self, x = 0.0, y = 0.0, heading = 0.0, u_turning = 2*pi/30, d_turning = -(2*pi/31), u_distance = 0.5, d_distance = 1.0, col_d_c = 1.0, col_a_c = 1.0, m_o = pi+(pi/2)):
        """This function is called when you create a new robot. It sets some of 
        the attributes of the robot, either to their default values or to the values
        specified when it is created."""
        self.x = x
        self.y = y
        self.heading = heading
        self.u_turning = u_turning # only applies to target robots who constantly move in a circle
        self.d_turning = d_turning # only applies to target robots who constantly move in a circle
        self.u_distance = u_distance # only applies to target bot, who always moves at same speed.
        self.d_distance = d_distance # only applies to target bot, who always moves at same speed.
        self.col_d_c = col_d_c
        self.col_a_c = col_a_c
        self.m_o = m_o # only applies to target bot, who always moves at same speed.
        self.u_turning_noise    = 0.0
        self.d_turning_noise    = 0.0
        self.u_distance_noise    = 0.0
        self.d_distance_noise    = 0.0
        self.col_d_c_noise = 0.0
        self.col_a_c_noise = 0.0
        self.hbw = None


    def set_noise(self, new_ut_noise, new_dt_noise, new_ud_noise, new_dd_noise, new_col_d_noise, new_col_a_noise):
        """This lets us change the noise parameters, which can be very
        helpful when using particle filters."""
        self.u_turning_noise    = float(new_ut_noise)
        self.d_turning_noise    = float(new_dt_noise)
        self.u_distance_noise    = float(new_ud_noise)
        self.d_distance_noise    = float(new_dd_noise)
        self.col_d_noise    = float(new_col_d_noise)
        self.col_a_noise    = float(new_col_a_noise)


    def move(self, turning, distance, tolerance = 0.001, max_turning_angle = pi):
        
        """This function turns the robot and then moves it forward."""
        # apply noise, this doesn't change anything if turning_noise
        # and distance_noise are zero.
        #turning = random.gauss(turning, self.turning_noise)
        #distance = random.gauss(distance, self.distance_noise)

        # truncate to fit physical limitations
        turning = max(-max_turning_angle, turning)
        turning = min( max_turning_angle, turning)
        distance = max(0.0, distance)

        # Execute motion
        if(turning != 0.0):
            #self.heading += turning
            #self.heading = angle_trunc(self.heading)
            o_p = self.heading + turning
            o_p = hu.angle_trunc(o_p)
        x_p = (self.x + distance * cos(self.heading))
        y_p = (self.y + distance * sin(self.heading))
        #print("Distance")
        #print(distance)
        v = []
        col_d_c = random.gauss(self.col_d_c, self.col_d_c_noise)
        col_a_c = random.gauss(self.col_a_c, self.col_a_c_noise)
        colission, v, h = self.hbw.eval_colission(self.x, self.y, x_p, y_p, self.heading, col_d_c, col_a_c)
        
        #print("count, x_p, y_p, v")
        #print(self.count, x_p, y_p, v)
        #self.count = self.count + 1
        
        if(colission):
            self.x = v[1][0]
            self.y = v[1][1]
            self.heading = h
        else:
            self.x = x_p
            self.y = y_p
            
            #print("COLISSION")
            #print(self.x, self.y)
            """TODO: WRITE COLISSION LOGIC"""
        if(turning != 0.0):
            o_p = self.heading + turning
            o_p = hu.angle_trunc(o_p)
            self.heading = o_p
        

    def d_move(self):
        turning = random.gauss(self.d_turning, self.d_turning_noise)
        distance = random.gauss(self.d_distance, self.d_distance_noise)
        self.move(turning, distance)
        self.m_o = 0.0
        
    def u_move(self):
        turning = random.gauss(self.u_turning, self.u_turning_noise)
        distance = random.gauss(self.u_distance, self.u_distance_noise)
        self.move(turning, distance)
        self.m_o = pi
        

    def eval_motor_state(self):
        #Initialize motion type...            
        motion_type = 1 #DOWN MOTION
        if(self.m_o < pi):
            motion_type = 0 # UP MOTION
            
        
    def resolve_motion(self, rot):
        
        """TODO: EMBED COLISSION DETECTION FOR EACH TYPE OF MOVEMENT"""
        #Initialize motion type...            
        motion_type = self.eval_motor_state()
        
        #UP MOTION
        if motion_type == 0:
            self.u_move()
        else:
            self.d_move()
        rot -= 0.50
        return rot
    def resolve_partial_motion(self, rot):
        """This function is used to advance the runaway target bot."""
        
        """TODO: EMBED COLISSION DETECTION FOR EACH TYPE OF MOVEMENT"""
        
        #Initialize motion type...            
        motion_type = self.eval_motor_state()
        #Calculate distance & update rotations
        dist_remainder = 0.0
        distance = 0.0
        turning = 0.0
        prior_m_o = self.m_o
        #UP MOTION
        if (motion_type == 0):
            
            dist_remainder = (pi)-self.m_o / (pi)
            
            if(rot - (dist_remainder/2) < 0.0):
                dist_remainder = rot*2
                rot = 0.0
                self.m_o += ((dist_remainder/2)*2*pi) % 2*pi
            else:
                rot -= (dist_remainder/2)   
                self.m_o = pi
            distance = self.u_distance * dist_remainder
        #DOWN MOTION
        else:
                
            dist_remainder = (pi*2)-self.m_o / (pi)
            
            #IF THE RATIO OF MOTOR POSITION AND HALF A ROTATION IS 0.5 (QUARTER-TURN REMAINING)
            #THEN IT FOLLOWS THAT IF 0.1 OF ROTATIONS REMAIN, (0.5 / 2 = 0.25) => (0.1 - 0.25 < 0.0)
            # 0.1 IS THE NEW BASIS FOR DISTANCE REMAINDER, 0.1 OF A FULL TURN IS 0.2 OF A HALF TURN
            # THUS dist_remainder = rot*2 IN THIS CASE...
            if(rot - (dist_remainder/2) < 0.0):
                dist_remainder = rot*2
                rot = 0.0
                self.m_o += ((dist_remainder/2)*2*pi) % 2*pi
            else:
                rot -= (dist_remainder/2)
                self.m_o = 0.0
            distance = self.d_distance * dist_remainder
            
            
        # NO TURN, JUST CONTINUE THE PRIOR MOTION
        self.move(0.0, (distance))
        
        #TURN IF 180 OR 360 DEGREES TRAVERSED
        if(self.m_o > 180 and prior_m_o < 180):
            self.move(self.d_turning, 0.0)
              
        if(self.m_o < prior_m_o):
            self.move(self.u_turning, 0.0)
                
        return rot
            

    def r_move(self, rot):        
        rot = self.resolve_partial_motion(rot)
        while(rot > 0.5):
            rot = self.resolve_motion(rot)
        if(rot > 0.0):
            self.resolve_partial_motion(rot)
            
    def sense(self):
        """This function represents the robot sensing its location. When
        measurements are noisy, this will return a value that is close to, 
        but not necessarily equal to, the robot's (x, y) position."""
        return (random.gauss(self.x, self.measurement_noise),
                random.gauss(self.y, self.measurement_noise))

    def __repr__(self):
        """This allows us to print a robot's position"""
        return '[%.5f, %.5f]'  % (self.x, self.y)
        
    """def get_hb(self):
        # Do something if you want
        return self.hb"""

    def set_hbw(self, hbw):
        # Do something if you want
        self.hbw = hbw
        



