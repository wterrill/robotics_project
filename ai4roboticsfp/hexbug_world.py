from math import *
from scipy.optimize import fsolve
from hexbug_utility import *
from hexbug_plotter import *
import numpy as np
import random

# helper function to map all angles onto [-pi, pi]
col_data = [[],[],[],[],[],[]]
def angle_trunc(a):
    while a < 0.0:
        a += pi * 2
    return ((a + pi) % (pi * 2)) - pi

class hexbug_world:

    def __init__(self, boundaries):
        """This function is called when you create a new robot. It sets some of 
        the attributes of the robot, either to their default values or to the values
        specified when it is created."""
        self.boundaries = boundaries


    def add_boundary(self, boundary):
        self.boundaries.append(boundary)


    def is_colission(self, x, y):
        colission = False
        b_r = []
        for b in self.boundaries:
            
            if b[0] == 'x':
                if b[1] == 'gt':
                    if x > b[2]:
                        colission = True
                        b_r = b
                else:
                    if x < b[2]:
                        colission = True
                        b_r = b
                        
            elif b[0] == 'y':
                if b[1] == 'gt':
                    if y > b[2]:
                        colission = True
                        b_r = b
                else:
                    if y < b[2]:
                        colission = True
                        b_r = b
                
            else:
                in_circle = ((x - b[1])**2 + (y - b[2])**2 < b[3]**2)
                if in_circle:
                    print("Circle Colission Coords")
                    print(x, y)
                    colission = True
                    b_r = b
            if(colission):
                break
            """TODO: IMPLEMENT LINE COLISSION LOGIC USING CROSS PRODUCT"""       
            #elif b[0] == 'line':
                
            #elif b[0] == 'circle':
        return colission, b_r
        
    def eval_colission(self, x1, y1, x2, y2, heading, col_d_c, col_a_c):
        v = []
        #v = [[x1, y1], [x2, y1]]
        #p = [x2, xy, orin]
        p = []
        #o = 0.0
        colission_init, b = self.is_colission(x2, y2)
        colission = colission_init
        
        if(colission_init):
            col_data[0].append(x2)
            col_data[1].append(y2)
        #if(colission_init):
                #print("NEW COLISSION")
            
        #colission = False
        i = 1
        while(colission):
            #lx_arr = np.arange(x1, x2)
            #m = 0.0
            #if(x2-x1 == 0):
            #    m = 999999
            #else:
            #    m = ((y2-y1)/(x2-x1))
            #q = y2-m*x2
            #ly_arr = m * lx_arr + q
            #i_p = self.eval_intersect(lx_arr, ly_arr, b)
            
            if b[0] == 'x':
                if b[1] == 'gt':
                    if x2 > b[2]:
                        #print("X GT POINT COLISSION")
                        v, heading = self.eval_point_colission(x1, y1, x2, y2, heading, col_d_c, col_a_c, b)
                else:
                    if x2 < b[2]:
                        #print("X LT POINT COLISSION")
                        v, heading = self.eval_point_colission(x1, y1, x2, y2, heading, col_d_c, col_a_c, b)
                        
            elif b[0] == 'y':
                if b[1] == 'gt':
                    if y2 > b[2]:
                        #print("Y GT POINT COLISSION")
                        v, heading = self.eval_point_colission(x1, y1, x2, y2, heading, col_d_c, col_a_c, b)
                else:
                    if y2 < b[2]:
                        #print("Y LT POINT COLISSION")
                        v, heading = self.eval_point_colission(x1, y1, x2, y2, heading, col_d_c, col_a_c, b)
            elif b[0] == 'line':
                print("LINE COLISSION")
                n = []
                #if we define dx=x2-x1 and dy=y2-y1, then the normals are (-dy, dx) and (dy, -dx).
                n.append(b[1])
            else:
                #CIRCLE LOGIC
                #m = (i_p[0]/sqrt(b[3]**2 - i_p[0]**2))
                print("CIRCLE COLISSION")
                v, heading = self.eval_tan_colission(x1, y1, x2, y2, heading, col_d_c, col_a_c, b)
            """TODO: IMPLEMENT LINE COLISSION LOGIC USING CROSS PRODUCT""" 
            colission, b = self.is_colission(v[1][0],v[1][1])
            #print("old & new x, y")
            #print(v[0][0],v[0][1])
            #print(v[1][0],v[1][1])
            if(not colission and i == 1):
                col_data[4].append(v[1][0])
                col_data[5].append(v[1][1])
            i = i + 1
                
            x1 = v[0][0]
            y1 = v[0][1]
            x2 = v[1][0]
            y2 = v[1][1]
            
            #elif b[0] == 'line':
                
            #elif b[0] == 'circle':
        return colission_init, v, heading

    def eval_point_colission(self, x1, y1, x2, y2, heading, col_d_c, col_a_c, b):
        dx = x2 - x1
        dy = y2 - y1
        #lx_arr = np.arange(x1, x2)
        
        intercept_x = 0.0
        intercept_y = 0.0
        heading_p = heading
        
        #vx_arr = np.arange(x1, x2)
        if(x2-x1 == 0):
            m = 999999
        else:
            m = ((dy)/(dx))
        q = y2-m*x2
        #vy_arr = m * vx_arr + q
        p_b = []
        v = []
        p_i = []
        p_b = []
        p_r = []
        p_h = []
        p_t = []
        
        if b[0] == 'x':
            intercept_x = b[2]
            intercept_y = m * intercept_x + q
            dx = x2 - intercept_x
            dy = y2 - intercept_y
            p_i = [intercept_x, intercept_y]
            p_b = [intercept_x - dx, intercept_y+dy]
            p_r = [intercept_x - (dx*col_d_c), intercept_y+(dy*col_d_c)]
            p_h = [intercept_x+cos(heading), intercept_y+sin(heading)]
            p_t = [intercept_x, intercept_y+sin(heading)]
                    
        elif b[0] == 'y':
            intercept_y = b[2]
            intercept_x = (intercept_y - q) / m
            dx = x2 - intercept_x
            dy = y2 - intercept_y
            p_i = [intercept_x, intercept_y]
            p_b = [intercept_x+dx, intercept_y-dy]
            p_r = [intercept_x+(dx*col_d_c), intercept_y-(dy*col_d_c)]
            p_h = [intercept_x+cos(heading), intercept_y+sin(heading)]
            p_t = [intercept_x+cos(heading), intercept_y]
            
        
        #p_i = [[x1, y1], [intercept_x, intercept_y]]
       
        #ang = np.arctan2([y1, ])
       
        d1 = np.asarray(p_h)-np.asarray(p_i);
        d2 = np.asarray(p_t) - np.asarray(p_i);
       
        angle1 = atan2(d1[1], d1[0])
        angle2 = atan2(d2[1], d2[0])
       
        a = angle2-angle1;
        if(abs(a) > (pi/4)):
            if(a > 0):
                a = (pi/2) - a
            else:
                a = (-pi/2) - a
        a = (a * col_a_c)
        heading_p = (heading_p + a)
        v.append(p_i)
        v.append(p_r)
        return v, heading_p
        
        
        
    def eval_tan_colission(self, x1, y1, x2, y2, heading, col_d_c, col_a_c, b):
        
        #lx_arr = np.arange(x1, x2)
        
        #lx_arr = np.arange(0, 1000)
        
        #Calculate data for top and bottom sections of semi circle
        #Shift these data to the center point of the circle
        #lx_arr_2 = np.arange(0 - b[3], 0 + b[3], 0.1)
        lx_arr_2 = np.arange(0 - b[3], 0 + b[3], 0.1)
        #lx_arr_rv = np.arange(0 - b[3]*2, 0 + b[3]*2, 0.1)
        semi_c_top = np.sqrt(b[3]**2 - lx_arr_2**2)
        semi_c_bottom = semi_c_top * -1.0
        semi_c_top = semi_c_top + b[2]
        semi_c_bottom = semi_c_bottom + b[2]
        lx_arr_2 = lx_arr_2 + b[1]
        #lx_arr_rv = lx_arr_rv + b[1]
        
        
        #Calculate deltas for slope of line segment intercepting circle...
        dx = x2 - x1
        dy = y2 - y1
        #Calculate line segment...
        m = 0.0
        if(dx == 0):
            m = 999999
        else:
            m = ((dy)/(dx))
        q = y2-m*x2
        ly_arr = m * lx_arr_2 + q
        for idx, val in enumerate(lx_arr_2):
            if val < x1:
                ly_arr[idx] = -100
            elif val > x2:
                ly_arr[idx] = -100
        
        #Gather points close to the intercept of the line segment and the semi circles...
        idx = []
        idx_avg_x = 0.0
        idx_avg_y = 0.0
        semi_c_y = []
        idx_t = (np.argwhere(np.isclose(semi_c_top, ly_arr, atol=1)).reshape(-1))
        idx_b = (np.argwhere(np.isclose(semi_c_bottom, ly_arr, atol=1)).reshape(-1))
        
        
        
        #If no data was gathered...
        #assume a colission on the left or right side of the circle s.t. tangent line is perfectly verticle
        
        #elif all intersection points come from the top semi circle, ignore the bottom...and the converse is true
        
        #else...evalute the distance between the points on the top and bottom semi circles gathered from the starting point
        #   of the hexbug, the closest cluster of points is assumed to be correct
        if(idx_t.size == 0 and idx_b.size == 0):
            xc = b[1]
            yc = b[2]
            dist_l = distance_between([xc-b[1], yc], [x1, y1])
            dist_r = distance_between([xc+b[1], yc], [x1, y1])
        
            if(dist_l < dist_r):
                b_c = ['x', 'gt', (xc-b[1])]
                
                v, heading_p = self.eval_point_colission(x1, y1, x2, y2, heading, col_d_c, col_a_c, b_c)
                colission, col_bound = self.is_colission(v[1][0],v[1][1])
                #i = 0
                print("LEFT SIDE CIRCLE COLISSION")
                if(colission):
                    col_data[2].append(v[1][0])
                    col_data[3].append(v[1][1])
                    #v, heading_p = self.eval_point_colission(x1, y1, x2, y2, heading, (col_d_c+(0.1*i)), col_a_c, b_c)
                    #v[1] = v[1] * 1.0 + (0.1*i)
                    v[1][0] = x1
                    v[1][1] = y1
                    #colission, col_bound = self.is_colission(v[1][0],v[1][1])
                    print("new point, colission?, boundary")
                    print(v[1], colission, col_bound)
                   # i = i + 1
                return v, heading_p
            else:
                b_c = ['x', 'lt', (xc+b[1])]
                
                v, heading_p = self.eval_point_colission(x1, y1, x2, y2, heading, col_d_c, col_a_c, b_c)
                colission, col_bound = self.is_colission(v[1][0],v[1][1])
                i = 0
                print("RIGHT SIDE CIRCLE COLISSION")
                if(colission):
                    col_data[2].append(v[1][0])
                    col_data[3].append(v[1][1])
                    #v[1] = v[1] * 1.0 + (0.1*i)
                    v[1][0] = x1
                    v[1][1] = y1
                    #v, heading_p = self.eval_point_colission(x1, y1, x2, y2, heading, (col_d_c+(0.1*i)), col_a_c, b_c)
                    #colission, col_bound = self.is_colission(v[1][0],v[1][1])
                    print("new point, colission?, boundary")
                    print(v[1], colission, col_bound)
                    #secondary_colission_
                    #i = i + 1
                return v, heading_p
        elif(idx_t.size == 0):
            idx = idx_b
            idx_avg_x_b = int(np.average(lx_arr_2[idx_b]))
            idx_avg_y_b = int(np.average(semi_c_bottom[idx_b]))
            idx_avg_x = idx_avg_x_b
            idx_avg_y = idx_avg_y_b
            semi_c_y = semi_c_bottom
        elif(idx_b.size == 0):
            idx = idx_t
            idx_avg_x_t = int(np.average(lx_arr_2[idx_t]))
            idx_avg_y_t = int(np.average(semi_c_top[idx_t]))
            idx_avg_x = idx_avg_x_t
            idx_avg_y = idx_avg_y_t
            semi_c_y = semi_c_top
        else:
            idx_avg_x_t = int(np.average(lx_arr_2[idx_t]))
            idx_avg_y_t = int(np.average(semi_c_top[idx_t]))
            idx_avg_x_b = int(np.average(lx_arr_2[idx_b]))
            idx_avg_y_b = int(np.average(semi_c_bottom[idx_b]))
            dist_t = distance_between([idx_avg_x_t, idx_avg_y_t], [x1, y1])
            dist_b = distance_between([idx_avg_x_b, idx_avg_y_b], [x1, y1])
        
            if(dist_t < dist_b):
                idx = idx_t
                idx_avg_x = idx_avg_x_t
                idx_avg_y = idx_avg_y_t
                semi_c_y = semi_c_top
            else:
                idx = idx_b
                idx_avg_x = idx_avg_x_b
                idx_avg_y = idx_avg_y_b
                semi_c_y = semi_c_bottom
        
        #idx_avg_x = int(np.average(lx_arr_2[idx]))
        #idx_avg_y = int(np.average(semi_c_bottom[idx]))
        
        ivec = [x1-x2, y1-y2]
        
        #ly_arr_relfection = 
        
        #p_h = [intercept_x+cos(heading), intercept_y+sin(heading)]
        #p_t = [intercept_x, intercept_y+sin(heading)]
        
        lx_arr_perp = np.arange(idx_avg_x - 50, idx_avg_x + 50)
        tan_params = np.polyfit(lx_arr_2[idx], semi_c_y[idx], 1)
        
        tly_arr = tan_params[0] * lx_arr_perp + tan_params[1]
        q = idx_avg_y-(idx_avg_x*(-1/tan_params[0]))
        tly_perp_arr = (-1/tan_params[0]) * lx_arr_perp + q
        
        tvec = [lx_arr_perp[-1] - lx_arr_perp[0],tly_arr[-1] - tly_arr[0]]
        norm = perpendicular(normalize(tvec))
        rvec = reflect(ivec, norm)
        v = [idx_avg_x, idx_avg_y], [(idx_avg_x-rvec[0]*col_d_c), (idx_avg_y-rvec[1]*col_d_c)]
        
        v_i = [[x1, y1], [idx_avg_x, idx_avg_y]]
       
        #ang = np.arctan2([y1, ])
       
        p_h = [idx_avg_x+cos(heading), idx_avg_y+sin(heading)]
        p_t1 = [idx_avg_x+1, tly_arr[(np.where(lx_arr_perp==(idx_avg_x+1)))]]
        p_t2 = [idx_avg_x-1, tly_arr[(np.where(lx_arr_perp==(idx_avg_x-1)))]]
        #p_n = [idx_avg_x+cos(heading), idx_avg_y]
        
        d1 = np.asarray(p_h)-np.asarray(v_i[1]);
        dt1 = np.asarray(p_t1) - np.asarray(v_i[1]);
        dt2 = np.asarray(p_t2) - np.asarray(v_i[1]);
       
        angle_h = atan2(d1[1], d1[0])
        angle_t_1 = atan2(dt1[1], dt1[0])
        angle_t_2 = atan2(dt2[1], dt2[0])
        
        #a = min(angle_t_1-angle_h, angle_t_2-angle_h)
        if(abs(angle_t_1-angle_h) < abs(angle_t_2-angle_h)):
            a = angle_t_1-angle_h
        else:
            a = angle_t_2-angle_h
       
        #a = angle2-angle_h;
        
        if(abs(a) > (pi/4)):
            if(a > 0):
                a = (pi/2) - a
            else:
                a = (-pi/2) - a
        a = (a * col_a_c)
        heading = (heading+a)
        
        colission, _ = self.is_colission(v[1][0],v[1][1])
        i = 0
        if(colission):
            hbp = hexbug_plotter(self.boundaries, col_d_c, col_a_c)
            hbp.visualize_circular_colission(x1, y1, x2, y2, heading, b)
            hbp.show_plots()
            print("CASCADING CIRCULAR COLISSION")
            #v = [idx_avg_x, idx_avg_y], [(idx_avg_x-rvec[0]*(col_d_c+(0.1*i))), (idx_avg_y-rvec[1]*(col_d_c+(0.1*i)))]
            #v[1][0] = v[1][0] * 1.0 + (0.1*i)
            #v[1][1] = v[1][1] * 1.0 + (0.1*i)
            #colission, col_bound = self.is_colission(v[1][0],v[1][1])
            #i = i + 1
            #print("new point, colission?, boundary")
            #print(v[1], colission, col_bound)
            
            col_data[2].append(v[1][0])
            col_data[3].append(v[1][1])
            
            v[1][0] = v_i[0][0]
            v[1][1] = v_i[0][1]
            
        return v, heading

    #def set_hb(self, hb):
    #    self.hb = hb        
    def get_colission_data(self):
        return col_data      
        
        
    """def eval_intersect(self, lx_arr, ly_arr, b):
        
        x = lx_arr[-1]
        y = ly_arr[-1]
        idx = []
                
            
        if b[0] == 'x':
            if b[1] == 'gt':
                if x > b[2]:
                    result = findIntersection(numpy.sin,numpy.cos,0.0)
            else:
                if x < b[2]:
                    result = findIntersection(numpy.sin,numpy.cos,0.0)
                    
        elif b[0] == 'y':
            if b[1] == 'gt':
                if y > b[2]:
                    result = findIntersection(numpy.sin,numpy.cos,0.0)
            else:
                if y < b[2]:
                    result = findIntersection(numpy.sin,numpy.cos,0.0)
        elif b[0] == 'line':
            print("LINE COLISSION")
            n = []
            #if we define dx=x2-x1 and dy=y2-y1, then the normals are (-dy, dx) and (dy, -dx).
            n.append(b[1])
        else:
            #CIRCLE LOGIC
            semi_c_top = math.sqrt(b[3]**2 - lx_arr**2)
            semi_c_bottom = semi_c_top * -1.0
            semi_c_top = semi_c_top + b[2]
            semi_c_bottom = semi_c_bottom + b[2]
            
            idx.append(np.argwhere(np.isclose(semi_c_top, ly_arr, atol=10)).reshape(-1))
            idx.append(np.argwhere(np.isclose(semi_c_bottom, ly_arr, atol=10)).reshape(-1))
            
            
            result = findIntersection(numpy.sin,numpy.cos,0.0)   
            #elif b[0] == 'line':
                
            #elif b[0] == 'circle':
        return colission, b

    def findIntersection(fun1,fun2,x0):
     return fsolve(lambda x : fun1(x) - fun2(x),x0)"""
    
        
       


