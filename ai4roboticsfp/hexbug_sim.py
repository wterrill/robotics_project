import numpy as np
from hexbug import *
from hexbug_world import *
from hexbug_utility import *
import matplotlib.pyplot as plt
from math import *



def run_hexbug(hb, time_s, mes_ps, rp_measure):   
    mt = [[],[]]
    for i in range(0, (int(time_s*mes_ps))):
        hb.r_move(rp_measure)
        mt[0].append(hb.x)
        mt[1].append(hb.y)
    return mt

#x_init = np.random.rand(400)+100
#y_init = np.random.rand(400)+100
#o_init = np.random.rand(2*pi)

x_init = 200.0
y_init = 200.0
o_init = 0.0

time_s = 2.0
mes_ps = 60.0
rp_min = 1800.0
rp_sec = (rp_min/60.0)
rp_measure = (rp_sec/mes_ps)
#u_dist = 10.0
#d_dist = 12.0
#u_turn = (2*pi) / 30.0
#d_turn = -(2*pi) / 20.0
#u_turn = (2*pi) / 40.0
#d_turn = (2*pi) / 20.0
#col_d_c = 0.5
#col_a_c = 0.5
#col_d_n = 0.5
#col_a_n = 0.5
m_o = (pi+(pi/2))



boundaries = []
boundaries.append(['x', 'lt', 78])
boundaries.append(['x', 'gt', 561])
boundaries.append(['y', 'lt', 37])
boundaries.append(['y', 'gt', 326])
boundaries.append(['circle', 331, 181, 37])
hbw = hexbug_world(boundaries)
#hb = hexbug(x_init, y_init, o_init, u_turn, d_turn, u_dist, d_dist, col_d_c, col_a_c, m_o)
hb_list = []
mt_list = []
data_lists = []

for i in range(1, 10):
    fname = "test"+str(i).zfill(2)+".txt"
    data_lists.append(get_data_list("data/",fname))
    
for i in range(0, 100):
    hb = generate_hexbug(x_init, y_init, o_init)
    
    hb.set_hbw(hbw)
    #hbw.set_hb(hb)
    
    hb_list.append(hb)
    
    mt_list.append(run_hexbug(hb, time_s, mes_ps, rp_measure))
    
    



#print(mt)
hbpath_fig=plt.figure("Simulator Run")
plt.axis([0,600,0,400])
hbpath_ax=hbpath_fig.add_subplot(1,1,1)
circ = plt.Circle((boundaries[4][1],boundaries[4][2]), radius=boundaries[4][3], color='g', fill=False)
hbpath_ax.add_patch(circ)
#hbpath_ax.scatter(mt[0], mt[1])
hbpath_ax.scatter(mt_list[0][0], mt_list[0][1], color='red')
hbpath_ax.scatter(mt_list[1][0], mt_list[1][1], color='blue')
hbpath_ax.scatter(mt_list[2][0], mt_list[2][1], color='green')
hbpath_ax.axvline(x=boundaries[0][2])
hbpath_ax.axvline(x=boundaries[1][2])
hbpath_ax.axhline(y=boundaries[2][2])
hbpath_ax.axhline(y=boundaries[3][2])

col_data = hbw.get_colission_data()

hbcol_fig=plt.figure("Colission Map")
plt.axis([0,600,0,400])
hbcol_ax=hbcol_fig.add_subplot(1,1,1)
circ2 = plt.Circle((boundaries[4][1],boundaries[4][2]), radius=boundaries[4][3], color='g', fill=False)
hbcol_ax.add_patch(circ2)
#hbpath_ax.scatter(mt[0], mt[1])
hbcol_ax.scatter(col_data[0], col_data[1], color='blue')
hbcol_ax.scatter(col_data[2], col_data[3], color='red')
hbcol_ax.scatter(col_data[4], col_data[5], color='green')
hbcol_ax.axvline(x=boundaries[0][2])
hbcol_ax.axvline(x=boundaries[1][2])
hbcol_ax.axhline(y=boundaries[2][2])
hbcol_ax.axhline(y=boundaries[3][2])

# TODO: MODIFY HEADING AS A FUNCTION OF CUR HEADING, MIN (TAN/NORM & INCIDENCE ANGLE DIFF), COL_ANG_COEFF

plt.show()
    

"""plt.subplot(321)
plt.scatter(x, y, s=80, c=z, marker=">")

plt.subplot(322)
plt.scatter(x, y, s=80, c=z, marker=(5, 0))

verts = list(zip([-1., 1., 1., -1.], [-1., -1., 1., -1.]))
plt.subplot(323)
plt.scatter(x, y, s=80, c=z, marker=(verts, 0))
# equivalent:
#plt.scatter(x,y,s=80, c=z, marker=None, verts=verts)

plt.subplot(324)
plt.scatter(x, y, s=80, c=z, marker=(5, 1))

plt.subplot(325)
plt.scatter(x, y, s=80, c=z, marker='+')

plt.subplot(326)
plt.scatter(x, y, s=80, c=z, marker=(5, 2))

plt.show()"""