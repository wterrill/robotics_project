from math import *
from matrix import matrix
import argparse
import visualize_twofiles

# ------------- UTIL FUNCTIONS -----------------------------------------------------------------------------------

# helper function to map all angles onto [-pi, pi]
def angle_truncate(a):
        while a < 0.0:
            a += pi * 2
        return ((a + pi) % (pi * 2)) - pi

def compute_error(pred_pt, act_pt):
    sum_delta_2 = 0
    num_data_points = len(pred_pt)
    # check if both arrays are the same size, and truncate if need be.
    if len(pred_pt) != len(act_pt):
        print "prediction_points do not equal actual_points, truncating"
        if len(act_pt) < len(pred_pt):
            num_data_points = len(act_pt)
    for i in range(num_data_points):
        delta_x_2 = (pred_pt[i][0] - act_pt[i][0]) ** 2
        delta_y_2 = (pred_pt[i][1] - act_pt[i][1]) ** 2

        sum_delta_2 = sum_delta_2 + delta_x_2 + delta_y_2
    error = sqrt(sum_delta_2)
    return error

def make_matrix(filename):
    list = []
    lines = open(filename).read().splitlines()
    for line in lines:
        list.append([int(line.split(",")[0]),int(line.split(",")[1])])
    return list

def save_predictions(results,number):
    #Saves result to prediction.txt
    with open('prediction'+str(number)+'.txt', 'w') as fout:
        for item in results:
            text = str(item[0]) + "," + str(item[1]) + "\n"
            fout.write(text)

def save_generated(results,i):
    #Saves the results to generated.txt file, only used for showing the last couple of seconds of data in plot
    with open('generated'+str(i)+'.txt', 'w') as fout:
        for i in range(1600, len(results)):
            text = str(results[i][0]) + "," + str(results[i][1]) + "\n"
            fout.write(text)

def distance_between(point1, point2):
    """Computes distance between point1 and point2. Points are (x, y) pairs."""
    x1, y1 = point1
    x2, y2 = point2
    return sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

def get_walls(measurements):
    #this just gets the boundarys for the walls based on the highest and lowest x and y values seen
    x  = []
    y = []
    for i in  range(len(measurements)):
        x.append(measurements[i][0])
        y.append(measurements[i][1])
    return min(x),  max(x),  min(y),  max(y)

def get_candle_boundaries(measurements,box_bound):
    # this gets the center of the candle as well as the radius.
    # The radius is computed by finding the minimum distance from the center,
    # and then rounding up to the next whole number
    distances = []
    center = (((box_bound[1]-box_bound[0])/2)+box_bound[0],((box_bound[3]-box_bound[2])/2)+box_bound[2])
    for  point in measurements:
        distances.append(distance_between(point,center))
    radius = min(distances)
    radius = ceil(radius)
    print radius
    return center,radius

def get_bearing(center, location):
    #this is used to get the bearing to a certain point.. used to reflect the angle on the candle
    diff_x = center[0] - location[0]
    diff_y = center[1] - location[1]
    angle = atan2(diff_y,diff_x)
    return (angle+pi)%(2*pi)

# ------------- END UTIL FUNCTIONS -----------------------------------------------------------------------------------

def estimate_next_pos(measurement_target, OTHER = None):



    #identity matrix for Kalman Filter
    I = matrix([[1, 0, 0],
                [0, 1, 0],
                [0, 0, 1]])

    # seed for noise
    noise = 0.05
    R = matrix([[noise, 0],
                [0, noise]])

    if OTHER is not None:
        measurement_last = OTHER['measurement_last']

        heading = atan2(measurement_target[1] - measurement_last[1], measurement_target[0] - measurement_last[0])

        # Load the current state and covariance matrices for KF
        X = OTHER['X'] #state
        P = OTHER['P'] #covariance
        xy_estimate = []

        if 'heading_last' not in OTHER:
            OTHER['counter'] = 1 # keep track of the number of cycles
            OTHER['measurement_last'] = measurement_target # where did we come from
            OTHER['heading_last'] = heading # where we were headed
            OTHER['distance_last'] = 0 # start off with zero
            OTHER['turn_decay'] = 0.94
        else:
            OTHER['counter'] += 1 #count up
            turning_angle = heading - OTHER['heading_last']
            OTHER['measurement_last'] = measurement_target
            OTHER['heading_last'] = heading

            # calculate a general speed of the bot.
            distance_travelled = distance_between(measurement_target, measurement_last)
            step = OTHER['counter']
            OTHER['distance_last'] = (OTHER['distance_last']*step + distance_travelled)/(step + 1)

# Predict:
            turn_decay = OTHER['turn_decay']
            turning_angle = turning_angle * turn_decay # takes a percentage of the previous turning angle

            # Store and calculate the next robot heading, next_heading
            OTHER['last_turning_angle'] = turning_angle
            next_heading = (heading+turning_angle)%(2*pi)
            delta_x = distance_travelled * cos(next_heading)
            delta_y = distance_travelled * sin(next_heading)
            nextX = measurement_target[0] + delta_x
            nextY = measurement_target[1] + delta_y

            # Predicted state matrix X
            X = matrix([[next_heading],
                         [nextX],
                         [nextY]])

            F = matrix([[1, 0, 0],   # Jacobian of the State Transition Model (used in EKF)
                        [-distance_travelled*sin(next_heading), 1, 0],
                        [distance_travelled*cos(next_heading), 0, 1]])

            P = OTHER['P'] #needs to be updated from iteration to iteratino


            H = matrix([[0, 1, 0], # Jacobian of the Measurement Function (used in  EKF)
                        [0, 0, 1]])

            # Predicted covariance estimate
            P = F * P * F.transpose()
# Update Measurements

            observations = matrix([[measurement_target[0]],
                                   [measurement_target[1]]])
            #run through the Kalman filter calculations

            Z = H*X

            Y = observations - Z                # Measurement delta

            S = H * P * H.transpose() + R       # covariance

            K = P * H.transpose() * S.inverse() # Kalman Gain

            X = X + (K*Y)                       # Update state estimate

            P = (I - (K * H)) * P #  covariance estimate

# fit and store
            X.value[0][0] = angle_truncate(X.value[0][0])
            OTHER['X'] = X
            OTHER['P'] = P
            x_estimate = OTHER['X'].value[1][0]
            y_estimate = OTHER['X'].value[2][0]
            xy_estimate = [x_estimate, y_estimate]
    else:
        # initial guess for X
        X = matrix([[0],
                    [measurement_target[0]],
                    [measurement_target[1]]])
        # convariance matrix  with high uncertainty in next_heading
        P = matrix([[1000, 0, 0],
                    [0, 200, 0],
                    [0, 0, 200]])

        OTHER = {'measurement_last': measurement_target, 'X': X, 'P': P}
        xy_estimate = [X.value[1][0], X.value[2][0]]
    return xy_estimate, OTHER

def forward_extrapolate_move(target_measurement, next_heading, OTHER):

    distance_last = OTHER['distance_last'] #naively assume travel distance is the same per frame, as the last measurement

    # Get X, Y values for rectangular box.
    min_x = OTHER['boundaries'][0]
    max_x = OTHER['boundaries'][1]
    min_y = OTHER['boundaries'][2]
    max_y = OTHER['boundaries'][3]

    # Get the radius and the center of the candle
    radius = OTHER['candle_bound'][1]
    center_x = OTHER['candle_bound'][0][0]
    center_y = OTHER['candle_bound'][0][1]
    turn_decay = OTHER['turn_decay']

    delta_x = distance_last * cos(next_heading)
    delta_y = distance_last * sin(next_heading)

    nextX = target_measurement[0] + delta_x
    nextY = target_measurement[1] + delta_y

    # reflect angles off of the  walls
    if nextY <= min_y:  # collision with bottom wall
        nextY = min_y
        next_heading = 2 * pi - next_heading

    if nextY >= max_y:  # collision with top wall
        nextY = max_y
        next_heading = 2 * pi - next_heading

    if nextX <= min_x:  # collision with left wall
        nextX = min_x
        next_heading = pi - next_heading

    if nextX >= max_x:  # collision with right wall
        nextX = max_x
        next_heading = pi - next_heading

    # reflect off of candle
    distance = distance_between((center_x,center_y),(nextX,nextY))
    if distance < radius : #collision with candle
        next_heading = (get_bearing((center_x, center_y), (nextX, nextY)))

    OTHER['last_turning_angle'] = OTHER['last_turning_angle'] * turn_decay

    X = matrix([[next_heading + OTHER['last_turning_angle']],
                [nextX],
                [nextY]])

    return X, OTHER

def Extended_Kalman_Filter(data):
    OTHER = None
    pred = []
    for datum in data:
        guess, OTHER = estimate_next_pos(datum, OTHER)
        pred.append(guess)
    return pred, OTHER

def prediction(OTHER, measurement_last, boundaries, frames, candle_bound):
    results = []
    OTHER['boundaries'] = boundaries
    OTHER['candle_bound'] = candle_bound
    X = matrix([[OTHER['heading_last']],
                [measurement_last[0]],
                [measurement_last[1]]])
    for i in range(frames):
        X, OTHER = forward_extrapolate_move([X.value[1][0], X.value[2][0]], X.value[0][0], OTHER)
        results.append([int(round(X.value[1][0])), int(round(X.value[2][0]))])
    return results

if __name__ == "__main__":

    test = True
    start = 0

    #Parse command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--text', '-t')
    parser.add_argument('--debug', '-d')
    args = parser.parse_args()

    i = 1 + start
    filename = args.text
    if test == True:
        end = 11
    else:
        end = 2
    for i in range(1,end):
        if test == True:
            if i < 10:
                filename = "test0" + str(i) + ".txt"
            else:
                filename = "test" + str(i) + ".txt"

        input_data = make_matrix(filename)
        boundaries = get_walls(input_data)
        candle_boundaries = get_candle_boundaries(input_data,boundaries)
        test_data = input_data[:]
        past_predictions, OTHER = Extended_Kalman_Filter(test_data)
        predicted = prediction(OTHER, test_data[-1], boundaries, 60, candle_boundaries)
        save_predictions(predicted,i)
        save_generated(test_data,i)
        visualize_twofiles.draw_file('generated'+str(i)+'.txt','prediction'+str(i)+'.txt',boundaries,candle_boundaries)


