finalproject.py runs the following way

$ python finalproject.py test07.txt

or

$ python finalproject.py ./inputs/test07.txt

due to confusion we have added the following so it runs both ways, with and without the containing folder

    try:
        predict(filename)
    except:
        filename = './inputs/' + filename
        predict(filename)