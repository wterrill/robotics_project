import matplotlib.pyplot as plot
import pylab
from math import *

def draw_file(filename1,filename2):
    #load data file1
    data_arrayA=[]
    f = open(filename1)
    for line in iter(f):
        a=line.split(',')
        point=[float(a[0]),float(a[1])]
        data_arrayA.append(point)
    f.close()
    #disperse data from file1
    xA=[]
    yA=[]
    for i in range(len(data_arrayA)):
        xA.append(data_arrayA[i][0])
        yA.append(data_arrayA[i][1])
    # load data file2
    data_arrayB = []
    f = open(filename2)
    for line in iter(f):
        a = line.split(',')
        point = [float(a[0]), float(a[1])]
        data_arrayB.append(point)
    f.close()
    # disperse data from file1
    xB = []
    yB = []
    for i in range(len(data_arrayB)):
        xB.append(data_arrayB[i][0])
        yB.append(data_arrayB[i][1])
    # change 2 lines below to change time to show on plot
    # for example x[-10:], y[-10:] will show last 10 points
    xA=xA[-60:]
    yA=yA[-60:]
    # print 'max x-',max(x)#561
    # print 'max y-', max(y)#326
    # print 'min x-', min(x)#78
    # print 'min y-', min(y)#37
    #linex = [min(x),min(x),max(x),max(x),min(x)]
    #liney = [min(y), max(y),max(y),min(y),min(y)]
    #add 2 on max and subtract 2 on min
    font = {'family': 'serif',
            'color': 'darkred',
            'weight': 'normal',
            'size': 16,
            }
    linex = [76, 76, 563, 563, 76]
    liney = [35, 328,328,35,35]
    circx = floor(((561+78)/2)+12)
    circy = floor((326 + 37) / 2)
    circle = plot.Circle((circx, circy),37,  alpha=.2)
    #circle=plot.Circle((332,177),35,alpha =.2)
    ax=pylab.axes(aspect=1)
    plot.plot(linex, liney)

    ax.add_patch(circle)
    #s will change size of dot
    plot.scatter(xA, yA, s=2,color='red', label='observed  path')
    plot.scatter(xB, yB, s=2, color='blue', label='predicted path')
    #making dotted line to last point in data
    plot.plot(xA[-2:],yA[-2:],'--',color='green')
    plot.plot(xB[-2:], yB[-2:], '--', color='purple')
    # putting asterix on last point in data
    plot.plot(xA[-1], yA[-1], '*', color='green')
    plot.plot(xB[-1], yB[-1], '*', color='purple')
    plot.plot(xA, yA, '-', color='yellow')
    plot.plot(xB, yB, '-', color='orange')
    plot.title(filename1+' compared with '+filename2)
    #plot.title(name + ' ' + str(len(x)) + ' seconds')
    #plot.legend()
    #plot.xlabel('Hex-bug Challenge',fontdict=font)
    #plot.ylabel('y-axis',fontdict=font)
    plot.grid(True)
    #take out next three lines to just show on screen plot
    fig1 = plot.gcf()
    #just_name=name.split('.')
    just_name ="image"
    #fig1.savefig(just_name[0]+'_last_15_secs.png')
    fig1.savefig(just_name + '.png')
    #plot.clf()
    plot.show()

# draw_file('../finalproject/inputs/test06.txt','../finalproject/prediction.txt')
