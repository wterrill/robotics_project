#Contents of folder utilities

##mse.py.txt		

file calculates the MSE between a.txt and b.txt. it works by calling mse(file1,file2) and returns a number
change name of file1 and file2 to use function with other data

##a.txt & b.txt

test data used for calculating MSE

##visualize_twofiles.py

file plots two sets of data. Each data file plotted in a different color. Look at compare_data.png to see example of what is produced

##c.txt & d.txt

test data used for visualize_twofiles.py 
