import numpy as np

def mse(file1,file2):
    #for file1
    data_arrayA=[]
    f = open(file1)
    for line in iter(f):
       a=line.split(',')
       point=[float(a[0]),float(a[1])]
       data_arrayA.append(point)
    f.close()
    #separate x and y
    xA=[]
    yA=[]
    for i in range(len(data_arrayA)):
        xA.append(data_arrayA[i][0])
        yA.append(data_arrayA[i][1])
    #for file2
    data_arrayB=[]
    f = open(file2)
    for line in iter(f):
       a=line.split(',')
       point=[float(a[0]),float(a[1])]
       data_arrayB.append(point)
    f.close()
    #separate x and y
    xB=[]
    yB=[]
    for i in range(len(data_arrayB)):
        xB.append(data_arrayB[i][0])
        yB.append(data_arrayB[i][1])
    xAn=np.array(xA)
    xBn=np.array(xB)
    yAn=np.array(yA)
    yBn=np.array(yB)
    return ((xAn - xBn)**2 + (yAn - yBn)**2).mean()

print mse('a.txt','b.txt')
#function runs as mse(file1,file2) and returns a mean for that array