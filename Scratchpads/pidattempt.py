# ----------
# Part Three
#
# Now you'll actually track down and recover the runaway Traxbot.
# In this step, your speed will be about twice as fast the runaway bot,
# which means that your bot's distance parameter will be about twice that
# of the runaway. You can move less than this parameter if you'd
# like to slow down your bot near the end of the chase.
#
# ----------
# YOUR JOB
#
# Complete the next_move function. This function will give you access to
# the position and heading of your bot (the hunter); the most recent
# measurement received from the runaway bot (the target), the max distance
# your bot can move in a given timestep, and another variable, called
# OTHER, which you can use to keep track of information.
#
# Your function will return the amount you want your bot to turn, the
# distance you want your bot to move, and the OTHER variable, with any
# information you want to keep track of.
#
# ----------
# GRADING
#
# We will make repeated calls to your next_move function. After
# each call, we will move the hunter bot according to your instructions
# and compare its position to the target bot's true position
# As soon as the hunter is within 0.01 stepsizes of the target,
# you will be marked correct and we will tell you how many steps it took
# before your function successfully located the target bot.
#
# As an added challenge, try to get to the target bot as quickly as
# possible.

from robot import *
from math import *
from matrix import *
import random
from Tkinter import *

def run(params, twiddle_robot, OTHER):
    n=100
    twiddle_OTHER=OTHER
    err = 0.0
    CTE=twiddle_OTHER['distance_crosstrack_error']
    for i in range(n*2):
        diff_CTE=OTHER['distance_crosstrack_error'] - twiddle_OTHER['diff_crosstrack_error']
        CTE = twiddle_OTHER['distance_crosstrack_error']
        int_CTE = twiddle_OTHER['sum_crosstrack_error']
        int_CTE+=CTE
        steer=- params[0] * CTE - params[1] * diff_CTE - int_CTE * params[2]
        twiddle_robot=twiddle_robot.move(twiddle_robot,twiddle_OTHER['heading'],twiddle_OTHER['distance'],tolerance=0.001, max_turning_angle=pi)
        if i>=n:
            err += (CTE ** 2)
    return err
def run2(params2, twiddle_robot, OTHER):
    n=100
    twiddle_OTHER=OTHER
    err = 0.0
    CTE=twiddle_OTHER['heading_crosstrack_error']
    for i in range(n*2):
        diff_CTE=OTHER['heading_crosstrack_error'] - twiddle_OTHER['heading_diff_crosstrack_error']
        CTE = twiddle_OTHER['distance_crosstrack_error']
        int_CTE = twiddle_OTHER['heading_sum_crosstrack_error']
        int_CTE += CTE
        heading=- params2[0] * CTE - params2[1] * diff_CTE - int_CTE * params2[2]
        twiddle_robot=twiddle_robot.move(twiddle_robot,twiddle_OTHER['heading'],twiddle_OTHER['distance'],tolerance=0.001, max_turning_angle=pi)
        if i>=n:
            err += (CTE ** 2)
    return err

def next_move(hunter_position, hunter_heading, target_measurement, max_distance, OTHER=None):
    # This function will be called after each time the target moves.
    #I initialize my variables
    turning = 0.0
    distance = 0.0
    xy_estimate = 0.0, 0.0
    # I will keep my idea of exercise 2 of using OTHER as a dictionary
    #since motion is in place now will go ahead and plan a PID approach to the problem =)
    if not OTHER:  # if my dictionary does not exist I initialize the variables used later on
        OTHER = {}
        #OTHER['last_point'] = 0.0, 0.0
        #OTHER['distance_betw_points'] = 0.0
        #OTHER['sum_distance'] = 0.0
        #OTHER['amount_distance'] = 1.0
        #OTHER['last_heading'] = 0.0
        OTHER['heading_crosstrack_error']=0.0
        OTHER['distance_crosstrack_error'] = 0.0
        OTHER['diff_crosstrack_error'] = 0.0
        OTHER['heading_diff_crosstrack_error'] = 0.0
        OTHER['sum_crosstrack_error'] = 0.0
        OTHER['heading_sum_crosstrack_error'] = 0.0
        OTHER['distance'] = 0.0
        OTHER['heading'] = 0.0


    else:
        # I used three points for starting exercise #1 here I use 2
        # this will be point 1
        measure_one = hunter_position
        # this will be point 2
        # target=who i am looking for     #hunter=me
        measure_two = target_measurement
        # once we have two points I see what the distances are
        distancexp1 = measure_two[0] - measure_one[0]
        distanceyp1 = measure_two[1] - measure_one[1]
        firstdistance = distance_between(measure_two, measure_one)
        #OTHER['last_point'] = hunter_position[0], hunter_position[1]
        # I get the heading as obtained in previous exercise
        heading_now = atan2(distanceyp1, distancexp1)
        #heading error=
        # steering = -tau_p * CTE - tau_d * diff_CTE - tau_i * int_CTE
        heading_crosstrack_error=0.0
        heading_crosstrack_error=angle_trunc(heading_now - hunter_heading)
        #this will the derivative part and assuming the delta t = 1
        OTHER['diff_crosstrack_error'] = firstdistance - OTHER['distance_crosstrack_error']
        OTHER['heading_diff_crosstrack_error']=heading_crosstrack_error - OTHER['heading_diff_crosstrack_error']
        #this is used for the proportional part of PID
        OTHER['distance_crosstrack_error'] = firstdistance
        OTHER['heading_diff_crosstrack_error'] = heading_crosstrack_error
        #this is used for the integral part
        OTHER['sum_crosstrack_error']+= firstdistance
        OTHER['heading_sum_crosstrack_error'] += heading_crosstrack_error
        twiddle_robot = robot(hunter_position[0], hunter_position[1], OTHER['heading'], 2 * pi / 30, OTHER['distance'])
        n_params = 3
        dparams = [1.0 for row in range(n_params)]
        params = [0.0 for row in range(n_params)]
        best_error = run(params,twiddle_robot,OTHER)
        #this is for getting the distance
        while sum(dparams)>0.2:
            for i in range(len(params)):
                params[i] += dparams[i]
                err=run(params,twiddle_robot,OTHER)
                if err < best_error:
                    best_error=err
                    dparams[i]*=1.1
                else:
                    params[i]-=2.0*dparams[i]
                    err=run(params,twiddle_robot,OTHER)
                    if err<best_error:
                        best_error=err
                        dparams[i] *=1.1
                    else:
                        params[i] += dparams[i]
                        dparams[i] *= 0.9
        # this is for getting the steer
        dparams2 = [1.0 for row in range(n_params)]
        params2 = [0.0 for row in range(n_params)]
        best_error2 = run2(params2,twiddle_robot,OTHER)
        # this is for getting the steer
        while sum(dparams2) > 0.2:
            for i in range(len(params2)):
                params2[i] += dparams2[i]
                err2 = run(params2, twiddle_robot, OTHER)
                if err2 < best_error2:
                    best_error2 = err2
                    dparams2[i] *= 1.1
                else:
                    params2[i] -= 2.0 * dparams2[i]
                    err2 = run(params2,twiddle_robot,OTHER)
                    if err2 < best_error2:
                        best_error2 = err2
                        dparams2[i] *= 1.1
                    else:
                        params2[i] += dparams2[i]
                        dparams2[i] *= 0.9
        #distance = k[0] * dist_err + k[1] * dist_guess - k[2] * dist_diff
        #turning = k[3] * head_err + k[4] * head_guess + k [5] * head_diff
        #turning = params[0] * OTHER['crosstrack_error'] + params[1] * OTHER['diff_crosstrack_error'] + params[2] * OTHER['sum_crosstrack_error']
        distance=params[0] * OTHER['distance_crosstrack_error'] + params[1] * OTHER['diff_crosstrack_error'] + params[2] * OTHER['sum_crosstrack_error']
        OTHER['distance']=distance
        turning=params2[0] * OTHER['heading_crosstrack_error'] + params2[1] * OTHER['heading_diff_crosstrack_error'] + params2[2] * OTHER['heading_sum_crosstrack_error']
        OTHER['heading']=turning
    # The OTHER variable is a place for you to store any historical information about
    # the progress of the hunt (or maybe some localization information). Your return format
    # must be as follows in order to be graded properly.
    return turning, distance, OTHER


def distance_between(point1, point2):
    """Computes distance between point1 and point2. Points are (x, y) pairs."""
    x1, y1 = point1
    x2, y2 = point2
    return sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

# def demo_grading(hunter_bot, target_bot, next_move_fcn, OTHER = None):
#     """Returns True if your next_move_fcn successfully guides the hunter_bot
#     to the target_bot. This function is here to help you understand how we
#     will grade your submission."""
#     max_distance = 1.94 * target_bot.distance # 1.94 is an example. It will change.
#     separation_tolerance = 0.02 * target_bot.distance # hunter must be within 0.02 step size to catch target
#     caught = False
#     ctr = 0
#     #For Visualization
#     import turtle
#     window = turtle.Screen()
#     window.bgcolor('white')
#     chaser_robot = turtle.Turtle()
#     chaser_robot.shape('arrow')
#     chaser_robot.color('blue')
#     chaser_robot.resizemode('user')
#     chaser_robot.shapesize(0.3, 0.3, 0.3)
#     broken_robot = turtle.Turtle()
#     broken_robot.shape('turtle')
#     broken_robot.color('green')
#     broken_robot.resizemode('user')
#     broken_robot.shapesize(0.3, 0.3, 0.3)
#     size_multiplier = 15.0 #change Size of animation
#     chaser_robot.hideturtle()
#     chaser_robot.penup()
#     chaser_robot.goto(hunter_bot.x*size_multiplier, hunter_bot.y*size_multiplier-100)
#     chaser_robot.showturtle()
#     broken_robot.hideturtle()
#     broken_robot.penup()
#     broken_robot.goto(target_bot.x*size_multiplier, target_bot.y*size_multiplier-100)
#     broken_robot.showturtle()
#     measuredbroken_robot = turtle.Turtle()
#     measuredbroken_robot.shape('circle')
#     measuredbroken_robot.color('red')
#     measuredbroken_robot.penup()
#     measuredbroken_robot.resizemode('user')
#     measuredbroken_robot.shapesize(0.1, 0.1, 0.1)
#     broken_robot.pendown()
#     chaser_robot.pendown()
#     #End of Visualization
#     # We will use your next_move_fcn until we catch the target or time expires.
#     while not caught and ctr < 1000:
#         # Check to see if the hunter has caught the target.
#         hunter_position = (hunter_bot.x, hunter_bot.y)
#         target_position = (target_bot.x, target_bot.y)
#         separation = distance_between(hunter_position, target_position)
#         if separation < separation_tolerance:
#             print "You got it right! It took you ", ctr, " steps to catch the target."
#             caught = True
#
#         # The target broadcasts its noisy measurement
#         target_measurement = target_bot.sense()
#
#         # This is where YOUR function will be called.
#         turning, distance, OTHER = next_move_fcn(hunter_position, hunter_bot.heading, target_measurement, max_distance, OTHER)
#
#         # Don't try to move faster than allowed!
#         if distance > max_distance:
#             distance = max_distance
#
#         # We move the hunter according to your instructions
#         hunter_bot.move(turning, distance)
#
#         # The target continues its (nearly) circular motion.
#         target_bot.move_in_circle()
#         #Visualize it
#         measuredbroken_robot.setheading(target_bot.heading*180/pi)
#         measuredbroken_robot.goto(target_measurement[0]*size_multiplier, target_measurement[1]*size_multiplier-100)
#         measuredbroken_robot.stamp()
#         broken_robot.setheading(target_bot.heading*180/pi)
#         broken_robot.goto(target_bot.x*size_multiplier, target_bot.y*size_multiplier-100)
#         chaser_robot.setheading(hunter_bot.heading*180/pi)
#         chaser_robot.goto(hunter_bot.x*size_multiplier, hunter_bot.y*size_multiplier-100)
#         #End of visualization
#         ctr += 1
#         if ctr >= 1000:
#             print "It took too many steps to catch the target."
#     return caught
def demo_grading(hunter_bot, target_bot, next_move_fcn, OTHER=None):
    """Returns True if your next_move_fcn successfully guides the hunter_bot
    to the target_bot. This function is here to help you understand how we
    will grade your submission."""
    max_distance = 1.94 * target_bot.distance  # 1.94 is an example. It will change.
    separation_tolerance = 0.02 * target_bot.distance  # hunter must be within 0.02 step size to catch target
    caught = False
    ctr = 0

    # We will use your next_move_fcn until we catch the target or time expires.
    while not caught and ctr < 1000:

        # Check to see if the hunter has caught the target.
        hunter_position = (hunter_bot.x, hunter_bot.y)
        target_position = (target_bot.x, target_bot.y)
        separation = distance_between(hunter_position, target_position)
        if separation < separation_tolerance:
            print "You got it right! It took you ", ctr, " steps to catch the target."
            caught = True

        # The target broadcasts its noisy measurement
        target_measurement = target_bot.sense()

        # This is where YOUR function will be called.
        turning, distance, OTHER = next_move_fcn(hunter_position, hunter_bot.heading, target_measurement, max_distance,
                                                 OTHER)

        # Don't try to move faster than allowed!
        if distance > max_distance:
            distance = max_distance

        # We move the hunter according to your instructions
        hunter_bot.move(turning, distance)

        # The target continues its (nearly) circular motion.
        target_bot.move_in_circle()

        ctr += 1
        if ctr >= 1000:
            print "It took too many steps to catch the target."
    return caught


def angle_trunc(a):
    """This maps all angles to a domain of [-pi, pi]"""
    while a < 0.0:
        a += pi * 2
    return ((a + pi) % (pi * 2)) - pi


def get_heading(hunter_position, target_position):
    """Returns the angle, in radians, between the target and hunter positions"""
    hunter_x, hunter_y = hunter_position
    target_x, target_y = target_position
    heading = atan2(target_y - hunter_y, target_x - hunter_x)
    heading = angle_trunc(heading)
    return heading


def naive_next_move(hunter_position, hunter_heading, target_measurement, max_distance, OTHER):
    """This strategy always tries to steer the hunter directly towards where the target last
    said it was and then moves forwards at full speed. This strategy also keeps track of all
    the target measurements, hunter positions, and hunter headings over time, but it doesn't
    do anything with that information."""
    if not OTHER:  # first time calling this function, set up my OTHER variables.
        measurements = [target_measurement]
        hunter_positions = [hunter_position]
        hunter_headings = [hunter_heading]
        OTHER = (measurements, hunter_positions, hunter_headings)  # now I can keep track of history
    else:  # not the first time, update my history
        OTHER[0].append(target_measurement)
        OTHER[1].append(hunter_position)
        OTHER[2].append(hunter_heading)
        measurements, hunter_positions, hunter_headings = OTHER  # now I can always refer to these variables

    heading_to_target = get_heading(hunter_position, target_measurement)
    heading_difference = heading_to_target - hunter_heading
    turning = heading_difference  # turn towards the target
    distance = max_distance  # full speed ahead!
    return turning, distance, OTHER

target = robot(0.0, 10.0, 0.0, 2*pi / 30, 1.5)
#x-0.0  , y- 10.0,  heading- 0.0,   turning - 0.21  distance - 1.5
measurement_noise = .05*target.distance
target.set_noise(0.0, 0.0, measurement_noise)

hunter = robot(-10.0, -10.0, 0.0)

print demo_grading(hunter, target, naive_next_move)


test_target = robot(2.1, 4.3, 0.5, 2*pi / 34.0, 1.5)
x-2.1  , y- 4.3,  heading- 0.5,   turning - 0.18  distance - 1.5
measurement_noise = 0.05 * test_target.distance
test_target.set_noise(0.0, 0.0, measurement_noise)
test_target.turning_noise = 0.0, test_target.distance_noise = 0.0   test_target.measurement_noise = 0.075
print measurement_noise
demo_grading(estimate_next_pos, test_target)


