% Example LaTeX document for GP111 - note % sign indicates a comment
\documentclass[a4paper,12pt]{article}
%\documentstyle[11pt]{article}
% Default margins are too wide all the way around. I reset them here
\setlength{\topmargin}{-.5in}
\setlength{\textheight}{9in}
\setlength{\oddsidemargin}{.125in}
\setlength{\textwidth}{6.25in}
\setlength{\parindent}{0cm}
\usepackage{multirow}
\usepackage[table]{xcolor}
\usepackage{hyperref}
\usepackage{cleveref}
\usepackage{tcolorbox}
\usepackage{graphicx}
\usepackage{default_doc}
\usepackage{filecontents}
\usepackage{datatool,tabularx}
\usepackage{pgfplotstable, booktabs}
\usepackage{subcaption}

\hypersetup{
     colorlinks   = true,
     citecolor = black
}

\graphicspath{ {./} }


\newcommand{\pc}{\emph{Preamble Correlator }}
\newcommand{\vd}{\emph{Variable Delay }}
\newcommand{\deltaK}{\delta_{\bf{x_{0:k}^{i}}}}
\newcommand{\postdist}{p(\bf{x_{0:k}}|\bf{z_{1:k}})}
\newcommand\numberthis{\addtocounter{equation}{1}\tag{\theequation}}
% \newcommand{\postdist}{p(x_{0:k})}  % |z_{1:k})}

% \newcommand{\bcp}{\emph{Byte Code Processor }}
% \newcommand{\mc}{\emph{Memory Controller }}

\renewcommand{\familydefault}{\sfdefault}
\begin{document}

% \author{Phil Vallance}

\section{Bayesian Inference} \label{sec:bayesian}

Particle Filtering is based on Bayesian inference.  Bayesian inference is the mathematical relationship
that relates the posterior probability to the prior estimate, the measurement likelihood and the
previous estimates.  The fundamental relationship of Bayesian inference is encapsulated in Bayes' Rule
given in~\eqref{eq:bayes_rule}.  Much of the following derivation and discussion is based on \cite{fundamental}.

\begin{equation} \label{eq:bayes_rule}
\underbrace{p(\bf{x_{k}}|\bf{z_{1:k}})}_\text{posterior probability} = \frac{ \overbrace{\bf{p(z_{k}}|\bf{x_{k}})}^\text{likelihood} \overbrace{p(\bf{x_{k}}|\bf{z_{1:k-1}})}^\text{prior estimate} }{\underbrace{p(\bf{z_{k}}|\bf{z_{1:k-1}})}_\text{evidence} }
\end{equation}

Here, $\bf{x}$ represents the state vector and $\bf{z}$ represents the measurement vector.  The evidence or the
normalizing constant in the denominator of Bayes' rule is given in~\eqref{eq:evidence} as defined by
the law of total probability.  Particle filtering approximates this exact integral through random sampling of the
state space $\bf{x_{k}}$.  Note that there is no assumption made as to the underlying probability distributions.  Particle
Filters do not require Gaussian distributions.

\begin{equation} \label{eq:evidence}
\underbrace{p(\bf{z_{k}}|\bf{z_{1:k-1}})}_\text{evidence} =  \int {p({{\bf{z}}_k}|{{\bf{x}}_k}} )p({{\bf{x}}_k}|{{\bf{z}}_{1:k - 1}})d{{\bf{x}}_k}
\end{equation}

The prior estimate is defined in terms of the Chapman-Komogorov equation shown here in~\eqref{eq:prior_est}. Again this
this integral is approximated by random sampling of the state space $\bf{x_{k}}$.

\begin{equation} \label{eq:prior_est}
\underbrace {p({{\bf{x}}_k}|{{\bf{z}}_{1:k - 1}})}_{{\textrm{prior estimate}}} = \int {p({{\bf{x}}_k}|{{\bf{x}}_{k - 1}}} )p({{\bf{x}}_{k - 1}}|{{\bf{z}}_{1:k - 1}})d{{\bf{x}}_{k - 1}}\\
\end{equation}


Combining~\eqref{eq:prior_est} and~\eqref{eq:evidence} into~\eqref{eq:bayes_rule} yields the optimal Bayesian inference
system.  This uses the Chapman-Kolmogorov equation for the prediction step and Bayes' Rule for the updated step. The block
diagram of this filter is shown below in~\ref{fig:bayesian_inference}.  Bayesian inference is optimal.  However, the integrals
generally do not have closed form solutions for practical applications.

\begin{figure}[H]
\caption{Bayesian Inference}
\centering
\includegraphics[width=.90\textwidth]{bayesian_inference.pdf}
\label{fig:bayesian_inference}
\end{figure}

\section{Particle Filter} \label{sec:particle_filter}

Particle filters are an approximation to Bayesian inference that rely on numerical integration to solve for the prediction
and update steps.  The first approximation is to represent the posterior distribution using a set of samples or "particles".

\begin{equation} \label{eq:post_approx}
\hat{p}\left(\bf{x_{0:k}}|\bf{z_{1:k}}\right)=\frac{1}{N}\sum_{i=1}^{N}\delta_{\bf{x_{0:k}^{i}}}d\bf{x_{0:k}}
\end{equation}

Random samples are drawn from the posterior distribution.  Approximate the expectations of the posterior probability with
a finite sum. Particles must represent the most dense areas of the conditional probability surface for good convergence.
In general, it is not possible to sample directly from the posterior probability distribution, $\postdist$.  Instead,
draw from a known easy to sample proposal distribution, $q(\bf{x_{0:k}}|\bf{z_{1:k}})$.  This distribution is often chosen
to be $p(\bf{x_{k}} | \bf{x_{k-1}})$.  This results in~\eqref{eq:post_approx3}.

\begin{equation} \label{eq:post_approx2}
E(g({{\bf{x}}_{0:k}})) = \int {g({{\bf{x}}_{0:k}})p({{\bf{x}}_{0:k}}|{{\bf{z}}_{1:k}})} d{{\bf{x}}_{0:k}} \cong \frac{1}{N}\sum\limits_{i = 1}^N {g({{\bf{x}}^i}_{0:k})}
\end{equation}

\begin{equation} \label{eq:post_approx3}
E(g({{\bf{x}}_{0:k}})) = \int {g({{\bf{x}}_{0:k}})\frac{{p({{\bf{x}}_{0:k}}|{{\bf{z}}_{1:k}})}}{{q({{\bf{x}}_{0:k}}|{{\bf{z}}_{1:k}})}}} q({{\bf{x}}_{0:k}}|{{\bf{z}}_{1:k}})d{{\bf{x}}_{0:k}}
\end{equation}


~\eqref{eq:post_approx3} can be simplified using probability theory and Bayes' Rule.  This derivation is shown below in
~\eqref{eq:post_approx4}.

\begin{align*} \label{eq:post_approx4}
E(g({{\bf{x}}_{0:k}})) &= \int {g({{\bf{x}}_{0:k}})\frac{{p({{\bf{x}}_{0:k}}|{{\bf{z}}_{1:k}})}}{{q({{\bf{x}}_{0:k}}|{{\bf{z}}_{1:k}})}}} q({{\bf{x}}_{0:k}}|{{\bf{z}}_{1:k}})d{{\bf{x}}_{0:k}} \\
E(g({{\bf{x}}_{0:k}})) &= \int {g({{\bf{x}}_{0:k}})\frac{{p({{\bf{z}}_{1:k}}|{{\bf{x}}_{0:k}})p({{\bf{x}}_{0:k}})}}{{p({{\bf{z}}_{1:k}})q({{\bf{x}}_{0:k}}|{{\bf{z}}_{1:k}})}}} q({{\bf{x}}_{0:k}}|{{\bf{z}}_{1:k}})d{{\bf{x}}_{0:k}}\;:\;{\textrm{Bayes' Rule}}\\
&= \int {g({{\bf{x}}_{0:k}})\frac{{{w_k}({{\bf{x}}_{0:k}})}}{{p({{\bf{z}}_{1:k}})}}} q({{\bf{x}}_{0:k}}|{{\bf{z}}_{1:k}})d{{\bf{x}}_{0:k}}\\
{w_k}({{\bf{x}}_{0:k}}) &= \frac{{p({{\bf{z}}_{1:k}}|{{\bf{x}}_{0:k}})p({{\bf{x}}_{0:k}})}}{{q({{\bf{x}}_{0:k}}|{{\bf{z}}_{1:k}})}}\\
p({{\bf{z}}_{1:k}}) &= \int {p({{\bf{z}}_{1:k}},{{\bf{x}}_{0:k}})} d{{\bf{x}}_{0:k}}\\
&= \int {\frac{{p({{\bf{z}}_{1:k}}|{{\bf{x}}_{0:k}})p({{\bf{x}}_{0:k}})q({{\bf{x}}_{0:k}}|{{\bf{z}}_{1:k}})}}{{q({{\bf{x}}_{0:k}}|{{\bf{z}}_{1:k}})}}} d{{\bf{x}}_{0:k}}\\  \numberthis
&= \int {{w_k}({{\bf{x}}_{0:k}})q({{\bf{x}}_{0:k}}|{{\bf{z}}_{1:k}})d{{\bf{x}}_{0:k}}} \\
{\textrm{This yields}}\\
E(g({{\bf{x}}_{0:k}})) &= \frac{{\int {g({{\bf{x}}_{0:k}}){w_k}({{\bf{x}}_{0:k}})} q({{\bf{x}}_{0:k}}|{{\bf{z}}_{1:k}})d{{\bf{x}}_{0:k}}}}{{\int {{w_k}({{\bf{x}}_{0:k}})q({{\bf{x}}_{0:k}}|{{\bf{z}}_{1:k}})d{{\bf{x}}_{0:k}}} }}\\
\mathord{\buildrel{\lower3pt\hbox{$\scriptscriptstyle\frown$}}\over E} (g({{\bf{x}}_{0:k}})) &= \frac{{\frac{1}{N}\sum\limits_{i = 1}^N {g({{\bf{x}}^i}_{0:k}){w_k}({{\bf{x}}_{0:k}})} }}{{\frac{1}{N}\sum\limits_{i = 1}^N {{w_k}({{\bf{x}}_{0:k}})} }} = \sum\limits_{i = 1}^N {g({{\bf{x}}^i}_{0:k}){{\tilde w}_k}({{\bf{x}}_{0:k}})}
\end{align*}

The particle filter still has a weight equation, $w_{k}(\bf{x_{0:k}})$, that grows linearly with the number of simulation
steps.  The three parameters that grow linearly with time are given in~\eqref{eq:linear_growth}.

\begin{align*} \label{eq:linear_growth}
{w_k}({{\bf{x}}_{0:k}}) = \frac{{p({{\bf{z}}_{1:k}}|{{\bf{x}}_{0:k}})p({{\bf{x}}_{0:k}})}}{{q({{\bf{x}}_{0:k}}|{{\bf{z}}_{1:k}})}} \\
p({{\bf{z}}_{1:k}}|{{\bf{x}}_{0:k}}) = \prod\limits_{j = 1}^k {p({z_j}|{x_{j - 1}})} \numberthis \\
p({{\bf{z}}_{1:k}}|{{\bf{x}}_{0:k}}) = \prod\limits_{j = 1}^k {p({z_j}|{x_{j - 1}})} \\
\end{align*}

Need a recursive form of these equations to reduce computational workload. The derivations of the recursive weight equations
are shown below in


\begin{align*} \label{eq:recursive}
{w_k} &= \frac{{p({{\bf{z}}_{1:k}}|{{\bf{x}}_{0:k}})p({{\bf{x}}_{0:k}})}}{{q({{\bf{x}}_k}|{{\bf{x}}_{0:k - 1}},{{\bf{z}}_{1:k}})q({{\bf{x}}_{0:k - 1}}|{{\bf{z}}_{1:k - 1}})}}\\
{w_k} &= {w_{k - 1}}\frac{{p({{\bf{z}}_{1:k}}|{{\bf{x}}_{0:k}})}}{{p({{\bf{z}}_{1:k - 1}}|{{\bf{x}}_{0:k - 1}})}}\frac{{p({{\bf{x}}_{0:k}})}}{{p({{\bf{x}}_{0:k - 1}})}}\frac{1}{{q({{\bf{x}}_k}|{{\bf{x}}_{0:k - 1}},{{\bf{z}}_{1:k}})}}\\\
{w_k} &= {w_{k - 1}}\frac{{p({{\bf{z}}_{1:k}}|{{\bf{x}}_{0:k}})}}{{p({{\bf{z}}_{1:k - 1}}|{{\bf{x}}_{0:k - 1}})}}\frac{{p({x_0})\prod\limits_{j = 1}^k {p({x_j}|{x_{j - 1}})} }}{{p({x_0})\prod\limits_{j = 1}^{k - 1} {p({x_j}|{x_{j - 1}})} }}\frac{1}{{q({{\bf{x}}_k}|{{\bf{x}}_{0:k - 1}},{{\bf{z}}_{1:k}})}}\\
{w_k} &= {w_{k - 1}}\frac{{p({{\bf{z}}_{1:k}}|{{\bf{x}}_{0:k}})}}{{p({{\bf{z}}_{1:k - 1}}|{{\bf{x}}_{0:k - 1}})}}\frac{{p({x_k}|{x_{k - 1}})}}{1}\frac{1}{{q({{\bf{x}}_k}|{{\bf{x}}_{0:k - 1}},{{\bf{z}}_{1:k}})}} \numberthis \\
{w_k} &= {w_{k - 1}}\frac{{\prod\limits_{j = 1}^k {p({z_j}|{x_{j - 1}})} }}{{\prod\limits_{j = 1}^{k - 1} {p({z_j}|{x_{j - 1}})} }}\frac{{p({x_k}|{x_{k - 1}})}}{1}\frac{1}{{q({{\bf{x}}_k}|{{\bf{x}}_{0:k - 1}},{{\bf{z}}_{1:k}})}}\\
{w_k} &= {w_{k - 1}}\frac{{p({{\bf{z}}_k}|{{\bf{x}}_{k - 1}})p({x_k}|{x_{k - 1}})}}{{q({{\bf{x}}_k}|{{\bf{x}}_{0:k - 1}},{{\bf{z}}_{1:k}})}}\\
&{\textrm{if proposal distribution :}}\;\;q({{\bf{x}}_k}|{{\bf{x}}_{0:k - 1}},{{\bf{z}}_{1:k}}) = p({x_k}|{x_{k - 1}})\\
{w_k} &= {w_{k - 1}}p({{\bf{z}}_k}|{{\bf{x}}_{k - 1}})
\end{align*}

Now the particle filter is constructed into a recursive algorithm much like the Bayesian inference diagram shown above. The
full algorithm is shown below in~\ref{fig:particle_filter_diagram}. Notice the Resample Wheel block; this block performs
Sample Importance Resampling (SIR).  This allows the particle filter to retain particles that are more statistically
relevant to the approximation of the posterior probability distribution.

\begin{figure}[H]
\caption{Particle Filter Flow Graph}
\centering
\includegraphics[width=.75\textwidth]{particle_filter.pdf}
\label{fig:particle_filter_diagram}
\end{figure}

The particle filter relies on a good distribution of particles where some of them lie in a region of high posterior probability.
As the number of particles, $N$, tends to inifinity this is guaranteed. When $N$ is relatively small the bulk of particles
have low weight. The posterior probability is estimated with few \textbf{effective} particles. This is known as particle
degeneration. In this work particle degeneration was detected and then a new set of samples was drawn using the previous
mean $\bf{x}$ values and injected large variance to cover the posterior probability surface.

\section{Particle Filter - Parameter Estimation} \label{sec:particle_filter_params}

The particles used in this project have many latent variables (see Kenyon's discussion of his model). A particle filter
was used to optimize these parameters so that the particle behave similarily to the actual hexbug. The main difference
to the setup is that now Bayes' Rule takes the form given in~\eqref{eq:bayes_rule_mod}.

\begin{equation} \label{eq:bayes_rule_mod}
\underbrace{p(\bf{x_{k}, \theta}|\bf{z_{1:k}})}_\text{posterior probability} = \frac{ \overbrace{\bf{p(z_{k}}|\bf{x_{k}, \theta})}^\text{likelihood} \overbrace{p(\bf{x_{k}, \theta}|\bf{z_{1:k-1}})}^\text{prior estimate} }{\underbrace{p(\bf{z_{k}}|\bf{z_{1:k-1}})}_\text{evidence} }
\end{equation}

Now we are estimating the joint distribution of $\bf{x_{k}}$ and $\bf{\theta}$.  Notice that $\bf{\theta}$, is assumed
to converge to constant parameters that do not change from one time step to the other. This particle filter is derived in
the same manner as the particle filter previously described.  It starts with particle filter approximation given in
~\eqref{eq:post_approx_mod}

\begin{equation} \label{eq:post_approx_mod}
\hat{p}\left(\bf{x_{0:k}, \theta}|\bf{z_{1:k}}\right)=\frac{1}{N}\sum_{i=1}^{N}\delta_{\bf{x_{0:k}^{i}, \theta}}d\bf{x_{0:k}}
\end{equation}

The complete derivation follows in exactly the same steps as given for the particle filter and are not given here.  The particle
filter works in the same manner as presented in the block diagram shown in~\ref{fig:particle_filter_diagram}.  However, the
main difference is that the parameters of the model are also randomly selected.  The particle parameters affect the behaviour
of individual particle projections/predictions. If these predictions align closely with the next measurement then the
particle is highly likely to survive the resampling step. Otherwise, the particle is likely to die and that parameter space.\bigskip

The implemented particle filter went a step further by providing a kernel smoothing operation to effectively estimate the
parameter space variance in an iterative fashion.  This was based on the work done by Liu and West \cite{liu_west}

\begin{equation} \label{eq:kernel_smoothing}
p(\bf{\theta} | \bf{z_{1:k}}) = \sum_{i=1}^{N}\omega_{t}^{(i)}N(\bf{\theta} | \bf{m_{t}^{(i)}}, h^{2}V_{t})
\end{equation}

$N$ is a normal distribution with variance, $h^{2}V_{2}$, and current kernel locations, $m_{t}^{(i)}$. This kernal approximation allows
the particle filter to generate new samples given the current sample variance and means. \cite{liu_west} proposed a shrinkage of
the kernel locations by~\eqref{eq:kernal_shrinkage}, where $a = \sqrt{1-h^{2}}$ and $\bar{\theta}$ is the current parameter mean.

\begin{equation} \label{eq:kernal_shrinkage}
m_{t}^{(i)} = a\theta_{t}^{(i)} + (1 - a)\bar{\theta}_{t}
\end{equation}

The system effectively performs parameter adjustments of the particles (that survive resampling) using the current parameter
sample variances and means. As the particle filter runs real time, its parameter confidence should tend to increase
(lower parameter sample variance) and the distribution of parameter values should narrow.  This can result in particle
degeneration if the particle filter starts to converge to a local minimum. To combat this the system needs to be able to detect
particle degeneration and perform a resampling of the parameter space using the current mean values with much larger variances.


\clearpage
\begin{thebibliography}{1}

\bibitem{fundamental} Liu, Y., Zhi, D. "Fundamental Principles and Applications of Particle Filters"
{\em June 2006. Proceedings 6th World Congress on Intelligent Control and Automation}

\bibitem{liu_west} Liu, J., West, M "Combined parameter and state estimation in simulation-based filtering"
{\em Springer-Verlag, New York, 2001}


\end{thebibliography}

\end{document}
