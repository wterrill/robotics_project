from math import *
from scipy.optimize import fsolve
from hexbug_utility import *
import hexbug_plotter as hbp
import numpy as np
import random
import pdb

# helper function to map all angles onto [-pi, pi]
plot_mode = False
col_data = [[],[],[],[],[],[]]
def angle_trunc(a):
    while a < 0.0:
        a += pi * 2
    return ((a + pi) % (pi * 2)) - pi

def visualize_point_colission(x1, y1, x2, y2, heading, boundary, col_d_c):
        #v, a = eval_point_colission(x1, y1, x2, y2, heading, 0.5, 1.0, boundary)
        dx = x2 - x1
        dy = y2 - y1
        #lx_arr = np.arange(x1, x2)

        intercept_x = 0.0
        intercept_y = 0.0

        vx_arr = np.arange(x1, x2)
        m = ((dy)/(dx))
        q = y2-m*x2
        vy_arr = m * vx_arr + q
        p_i = []
        p_b = []
        p_r = []
        p_h = []
        p_t = []

        if boundary[0] == 'x':
            intercept_x = boundary[2]
            intercept_y = m * intercept_x + q
            dx = x2 - intercept_x
            dy = y2 - intercept_y

            p_b = [intercept_x - dx, intercept_y+dy]
            p_r = [intercept_x - (dx*col_d_c), intercept_y+(dy*col_d_c)]
            p_h = [intercept_x+cos(heading), intercept_y+sin(heading)]
            p_t = [intercept_x, intercept_y+sin(heading)]

        elif boundary[0] == 'y':
            intercept_y = boundary[2]
            intercept_x = (intercept_y - q) / m
            dx = x2 - intercept_x
            dy = y2 - intercept_y
            p_b = [intercept_x+dx, intercept_y-dy]
            p_r = [intercept_x+(dx*col_d_c), intercept_y-(dy*col_d_c)]
            p_h = [intercept_x+cos(heading), intercept_y+sin(heading)]
            p_t = [intercept_x+cos(heading), intercept_y]


        p_i = [[x1, y1], [intercept_x, intercept_y]]

        #ang = np.arctan2([y1, ])
        d1 = np.asarray(p_h)-np.asarray(p_i[1]);
        d2 = np.asarray(p_t) - np.asarray(p_i[1]);

        angle1 = atan2(d1[1], d1[0])
        angle2 = atan2(d2[1], d2[0])

        a = angle2-angle1;
        if(abs(a) > (pi/4)):
            if(a > 0):
                a = (pi/2) - a
            else:
                a = (-pi/2) - a
        #print("angle & border")
        #print(a)
        #print(boundary)

class hexbug_world:

    def __init__(self, boundaries):
        """This function is called when you create a new robot. It sets some of
        the attributes of the robot, either to their default values or to the values
        specified when it is created."""
        self.boundaries = boundaries


    def add_boundary(self, boundary):
        self.boundaries.append(boundary)


    def is_circular_colission(self, x, y, b_val):
        in_circle = ((x - b_val[1])**2 + (y - b_val[2])**2 < b_val[3]**2)
        colission = False
        if in_circle:
            #print("Circle Colission Coords")
            #print(x, y)
            colission = True
        return colission
    def is_colission(self, x, y):
        colission = False
        b_r = []
        for b_val in self.boundaries:

            if b_val[0] == 'x':
                if b_val[1] == 'gt':
                    if x > b_val[2]:
                        colission = True
                        b_r = b_val
                else:
                    if x < b_val[2]:
                        colission = True
                        b_r = b_val

            elif b_val[0] == 'y':
                if b_val[1] == 'gt':
                    if y > b_val[2]:
                        colission = True
                        b_r = b_val
                else:
                    if y < b_val[2]:
                        colission = True
                        b_r = b_val

            else:
                if(self.is_circular_colission(x, y, b_val)):
                    colission = True
                    b_r = b_val
            if(colission):
                break
            """TODO: IMPLEMENT LINE COLISSION LOGIC USING CROSS PRODUCT"""
            #elif b_val[0] == 'line':

            #elif b_val[0] == 'circle':
        return colission, b_r

    def eval_colission(self, x1, y1, x2, y2, heading, col_d_c, col_a_c):
        v = []
        #v = [[x1, y1], [x2, y1]]
        #p = [x2, xy, orin]
        p = []
        x1_input = x1
        y1_input = y1

        x2_input = x2
        y2_input = y2
        #o = 0.0
        colission_init, b_val = self.is_colission(x2, y2)
        colission = colission_init

        collision_list = []
        pos_list = []
        prior_pos_list = []

        if(colission_init):
            col_data[0].append(x2)
            col_data[1].append(y2)
        if(colission_init):
            print("NEW COLISSION")

        #colission = False
        i = 1
        num_collisions = 0
        while(colission):
            collision_list.append(b_val)
        # if colission:
            #lx_arr = np.arange(x1, x2)
            #m = 0.0
            #if(x2-x1 == 0):
            #    m = 999999
            #else:
            #    m = ((y2-y1)/(x2-x1))
            #q = y2-m*x2
            #ly_arr = m * lx_arr + q
            #i_p = self.eval_intersect(lx_arr, ly_arr, b_val)

            if b_val[0] == 'x':
                if b_val[1] == 'gt':
                    if x2 > b_val[2]:
                        print("X GT POINT COLISSION")
                        v, heading = self.eval_point_colission(x1, y1, x2, y2, heading, col_d_c, col_a_c, b_val)
                else:
                    if x2 < b_val[2]:
                        print("X LT POINT COLISSION")
                        v, heading = self.eval_point_colission(x1, y1, x2, y2, heading, col_d_c, col_a_c, b_val)

            elif b_val[0] == 'y':
                if b_val[1] == 'gt':
                    if y2 > b_val[2]:
                        print("Y GT POINT COLISSION")
                        v, heading = self.eval_point_colission(x1, y1, x2, y2, heading, col_d_c, col_a_c, b_val)
                else:
                    if y2 < b_val[2]:
                        print("Y LT POINT COLISSION")
                        v, heading = self.eval_point_colission(x1, y1, x2, y2, heading, col_d_c, col_a_c, b_val)
            elif b_val[0] == 'line':
                print("LINE COLISSION")
                n = []
                #if we define dx=x2-x1 and dy=y2-y1, then the normals are (-dy, dx) and (dy, -dx).
                n.append(b_val[1])
            else:
                #CIRCLE LOGIC
                #m = (i_p[0]/sqrt(b_val[3]**2 - i_p[0]**2))
                print("CIRCLE COLISSION")
                v, heading = self.eval_tan_colission(x1, y1, x2, y2, heading, col_d_c, col_a_c, b_val)
                prior_pos_list.append((x1, y1))
                pos_list.append((x2, y2))
                # visualize_point_colission(x1_input, y1_input, x2_input, y2_input, (pi*2*0.15), collision_list[0], col_d_c)

                # v, heading = self.eval_tan_colission(x1, y1, x2, y2, heading, col_d_c, col_a_c, b_val)
            """TODO: IMPLEMENT LINE COLISSION LOGIC USING CROSS PRODUCT"""
            colission, b_val_new = self.is_colission(v[1][0],v[1][1])
            #print("old & new x, y")
            #print(v[0][0],v[0][1])
            #print(v[1][0],v[1][1])
            if(not colission and i == 1):
                col_data[4].append(v[1][0])
                col_data[5].append(v[1][1])
            i = i + 1

            x1 = v[0][0]
            y1 = v[0][1]
            x2 = v[1][0]
            y2 = v[1][1]

            prior_pos_list.append((x1, y1))
            pos_list.append((x2, y2))

#            if y2 > 1000 or x2 > 1000:
#                pdb.set_trace()
            #if x2 is np.inf or y2 is np.inf:
            #    pdb.set_trace()

            #if x2 is np.nan or y2 is np.nan:
            #    pdb.set_trace()

            num_collisions += 1
            if num_collisions > 3:
                break

            b_val = b_val_new

            #elif b_val[0] == 'line':

            #elif b_val[0] == 'circle':
        return colission_init, v, heading

    def eval_point_colission(self, x1, y1, x2, y2, heading, col_d_c, col_a_c, b_val):
        dx = x2 - x1
        dy = y2 - y1
        #lx_arr = np.arange(x1, x2)

        intercept_x = 0.0
        intercept_y = 0.0
        heading_p = heading

        #vx_arr = np.arange(x1, x2)
        if(x2-x1 == 0):
            b_y = y2
            m = None
        else:
            m = ((dy)/(dx))
            b_y = y2-m*x2


        if m is not None and abs(m) < .00001:
            m = np.sign(m) * .00001
        #vy_arr = m * vx_arr + b_y
        p_b = []
        v = []
        p_i = []
        p_b = []
        p_r = []
        p_h = []
        p_t = []

        if b_val[0] == 'x':
            intercept_x = b_val[2]
            if m is None:
                intercept_y = b_y
            else:
                intercept_y = m * intercept_x + b_y

            if b_val[1] == 'gt':
                if x1 > b_val[2] and x2 > b_val[2]:
                    intercept_y = y2

            if b_val[1] == 'lt':
                if x1 <= b_val[2] and x2 <= b_val[2]:
                    intercept_y = y2

            dx = x2 - intercept_x
            dy = y2 - intercept_y
            p_i = [intercept_x, intercept_y]
            p_b = [intercept_x - dx, intercept_y+dy]
            p_r = [intercept_x - (dx*col_d_c), intercept_y+(dy*col_d_c)]
            p_h = [intercept_x+cos(heading), intercept_y+sin(heading)]
            p_t = [intercept_x, intercept_y+sin(heading)]

            #if abs(p_i[0]) > 1000 or abs(p_r[0]) > 1000:
            #    pdb.set_trace()

        elif b_val[0] == 'y':
            intercept_y = b_val[2]
            if m is None:
                intercept_x = x1
            else:
                intercept_x = (intercept_y - b_y) / m

            # check if both y's are out of bounds
            if b_val[1] == 'gt':
                if y1 > b_val[2] and y2 > b_val[2]:
                    intercept_x = x2

            if b_val[1] == 'lt':
                if y1 <= b_val[2] and y2 <= b_val[2]:
                    intercept_x = x2

            dx = x2 - intercept_x
            dy = y2 - intercept_y
            p_i = [intercept_x, intercept_y]
            p_b = [intercept_x+dx, intercept_y-dy]
            p_r = [intercept_x+(dx*col_d_c), intercept_y-(dy*col_d_c)]
            p_h = [intercept_x+cos(heading), intercept_y+sin(heading)]
            p_t = [intercept_x+cos(heading), intercept_y]

            #if abs(p_i[0]) > 1000 or abs(p_r[0]) > 1000:
            #    pdb.set_trace()



        #p_i = [[x1, y1], [intercept_x, intercept_y]]

        #ang = np.arctan2([y1, ])
        d1 = np.array(p_h) - np.array(p_i)
        d2 = np.array(p_t) - np.array(p_i)

        angle1 = atan2(d1[1], d1[0])
        angle2 = atan2(d2[1], d2[0])

        a = angle2-angle1;
        if(abs(a) > (pi/4)):
            if(a > 0):
                a = (pi/2) - a
            else:
                a = (-pi/2) - a
        a = (a * col_a_c)
        heading_p = (heading_p + a)
        v.append(p_i)
        v.append(p_r)
        return v, heading_p


    def eval_tan_colission(self, x1, y1, x2, y2, heading, col_d_c, col_a_c, b_val):

        interval = ((x2-x1)*0.1)
        if x2 == x1:
            lx_arr = np.array([x1])
            # print "x2 == x1"  # pdb.set_trace()
        elif(x2 - x1 > 0):
            lx_arr = np.arange(x1, x2, interval)
        else:
            lx_arr = np.arange(x1, x2, interval)

        """if(x2 - x1 > 0):
            lx_arr = np.arange(x1, x2, 0.1)
        else:
            lx_arr = np.arange(x1, x2, 0.1)"""

        lx_arr = np.append(lx_arr, x2)

        m_i, q_i = calc_line_params(x1, y1, x2, y2)
        ly_arr = m_i * lx_arr + q_i

        col_x = 0
        col_y = 0
        for idx, val in enumerate(lx_arr):
            if(self.is_circular_colission(val, ly_arr[idx], b_val)):
                col_x = val
                col_y = ly_arr[idx]
                break

        if(col_x == 0):
            print("error")

        ivec = np.asarray([x1-x2, y1-y2])


        perp_x_arr = np.arange(col_x - 50, col_x + 50)

        m_p, q_p = calc_line_params(col_x, col_y, b_val[1], b_val[2])
        perp_y_arr = m_p * perp_x_arr + q_p

        perp_vec = np.asarray([perp_x_arr[-1] - perp_x_arr[0],perp_y_arr[-1] - perp_y_arr[0]])

        rvec = reflect(ivec, normalize(perp_vec))
        v = [col_x, col_y], [(col_x-rvec[0]*col_d_c), (col_y-rvec[1]*col_d_c)]

        v_i = [[x1, y1], [col_x, col_y]]

        #ang = np.arctan2([y1, ])

        p_h = [col_x+cos(heading), col_y+sin(heading)]
        p_p1 = [col_x+1, perp_y_arr[(np.where(perp_x_arr==(col_x+1)))]]
        p_p2 = [col_x-1, perp_y_arr[(np.where(perp_x_arr==(col_x-1)))]]
        #p_n = [idx_avg_x+cos(heading), idx_avg_y]

        d1 = np.asarray(p_h)-np.asarray(v_i[1]);
        dp1 = np.asarray(p_p1) - np.asarray(v_i[1]);
        dp2 = np.asarray(p_p2) - np.asarray(v_i[1]);

        angle_h = atan2(d1[1], d1[0])
        angle_p_1 = atan2(dp1[1], dp1[0])
        angle_p_2 = atan2(dp2[1], dp2[0])

        #a = min(angle_t_1-angle_h, angle_t_2-angle_h)
        """if(abs(angle_p_1-angle_h) < abs(angle_p_2-angle_h)):
            a = angle_p_1-angle_h
        else:
            a = angle_p_2-angle_h"""

        if(abs(angle_p_1-angle_h) < abs(angle_p_2-angle_h)):
            a = angle_h-angle_p_1
        else:
            a = angle_h-angle_p_2

        #a = angle2-angle_h;

        if(abs(a) > (pi/4)):
            if(a > 0):
                a = (pi/2) - a
            else:
                a = (-pi/2) - a
        a = (a * col_a_c)


        colission = self.is_circular_colission(v[1][0],v[1][1],b_val)
        i = 0
        if(colission):
            if(plot_mode):
                hbp = hexbug_plotter(self.boundaries, col_d_c, col_a_c)
                hbp.visualize_circular_colission_2(x1, y1, x2, y2, heading, col_d_c, col_a_c, b_val)
                hbp.show_plots()
            # print("CASCADING CIRCULAR COLISSION")
            #v = [idx_avg_x, idx_avg_y], [(idx_avg_x-rvec[0]*(col_d_c+(0.1*i))), (idx_avg_y-rvec[1]*(col_d_c+(0.1*i)))]
            #v[1][0] = v[1][0] * 1.0 + (0.1*i)
            #v[1][1] = v[1][1] * 1.0 + (0.1*i)
            #colission, col_bound = self.is_colission(v[1][0],v[1][1])
            #i = i + 1
            #print("new point, colission?, boundary")
            #print(v[1], colission, col_bound)

            col_data[2].append(v[1][0])
            col_data[3].append(v[1][1])

            v[1][0] = v_i[0][0]
            v[1][1] = v_i[0][1]

        heading = (heading+a) % (2*pi)
        return v, heading

    #def set_hb(self, hb):
    #    self.hb = hb
    def get_colission_data(self):
        return col_data


    """def eval_intersect(self, lx_arr, ly_arr, b_val):

        x = lx_arr[-1]
        y = ly_arr[-1]
        idx = []


        if b_val[0] == 'x':
            if b_val[1] == 'gt':
                if x > b_val[2]:
                    result = findIntersection(numpy.sin,numpy.cos,0.0)
            else:
                if x < b_val[2]:
                    result = findIntersection(numpy.sin,numpy.cos,0.0)

        elif b_val[0] == 'y':
            if b_val[1] == 'gt':
                if y > b_val[2]:
                    result = findIntersection(numpy.sin,numpy.cos,0.0)
            else:
                if y < b_val[2]:
                    result = findIntersection(numpy.sin,numpy.cos,0.0)
        elif b_val[0] == 'line':
            print("LINE COLISSION")
            n = []
            #if we define dx=x2-x1 and dy=y2-y1, then the normals are (-dy, dx) and (dy, -dx).
            n.append(b_val[1])
        else:
            #CIRCLE LOGIC
            semi_c_top = math.sqrt(b_val[3]**2 - lx_arr**2)
            semi_c_bottom = semi_c_top * -1.0
            semi_c_top = semi_c_top + b_val[2]
            semi_c_bottom = semi_c_bottom + b_val[2]

            idx.append(np.argwhere(np.isclose(semi_c_top, ly_arr, atol=10)).reshape(-1))
            idx.append(np.argwhere(np.isclose(semi_c_bottom, ly_arr, atol=10)).reshape(-1))


            result = findIntersection(numpy.sin,numpy.cos,0.0)
            #elif b_val[0] == 'line':

            #elif b_val[0] == 'circle':
        return colission, b_val

    def findIntersection(fun1,fun2,x0):
     return fsolve(lambda x : fun1(x) - fun2(x),x0)"""





