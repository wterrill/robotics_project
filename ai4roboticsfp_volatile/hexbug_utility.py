from math import *
import numpy as np
import hexbug as hb
import random


def calc_line_params(x1, y1, x2, y2):
    dx = float(x2 - x1)
    dy = float(y2 - y1)
    #Calculate line segment...
    m = 0.0
    if(dx == 0):
        m = 999999
    else:
        m = ((dy)/(dx))
    q = y2-m*x2
    return m, q
def generate_hexbug(x, y, o):
    x = x
    y = y
    o = random.gauss(o, (2*pi/10))%(2*pi)
    u_turning = random.gauss(0.0, (2*pi/10)) 
    d_turning = random.gauss(0.0, (2*pi/10))
    u_distance = abs(random.gauss(0.0, 10.0))+5.0
    d_distance = abs(random.gauss(0.0, 10.0))+5.0
    col_d_c = abs(random.gauss(0.5, 0.5))
    col_a_c = abs(random.gauss(1.0, 1.0))
    m_o = random.gauss(0.0, pi)
    h = hb.hexbug(x, y, o, u_turning, d_turning, u_distance, d_distance, col_d_c, col_a_c, m_o)
    
    return h
def angle_trunc(a):
    while a < 0.0:
        a += pi * 2
    return ((a + pi) % (pi * 2)) - pi
def get_data_list(directory, filename):
    data = [[],[]]
    file=open(directory+filename,'r') 

    row = file.readlines()
    
    for line in row:
        info = line.split(",")
        x = float(info[0])
        y = float(info[1])
        data[0].append(x)
        data[1].append(y)
    return data
    
def distance_between(point1, point2):
    """Computes distance between point1 and point2. Points are (x, y) pairs."""
    x1, y1 = point1
    x2, y2 = point2
    return sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

def perpendicular( a ) :
    b = np.empty_like(a)
    b[0] = -a[1]
    b[1] = a[0]
    return b

def normalize(a):
    a = np.array(a)
    return a/np.linalg.norm(a)
    
def reflect(vector, normal):
    return vector - 2 * np.dot(vector, normal) * normal;
    
def dot(vA, vB):
    return vA[0]*vB[0]+vA[1]*vB[1]
def ang_():
        d1 = np.asarray(p_r)-np.asarray(p_i[1]);
        d2 = np.asarray(p_i[0]) - np.asarray(p_i[1]);
       
        angle1 = atan2(d1[1], d1[0])
        angle2 = atan2(d2[1], d2[0])
        
        a = angle2 - angle1
        return a
        
"""def ang(lineA, lineB):
    # Get nicer vector form
    vA = [(lineA[0][0]-lineA[1][0]), (lineA[0][1]-lineA[1][1])]
    vB = [(lineB[0][0]-lineB[1][0]), (lineB[0][1]-lineB[1][1])]
    # Get dot prod
    dot_prod = dot(vA, vB)
    # Get magnitudes
    magA = dot(vA, vA)**0.5
    magB = dot(vB, vB)**0.5
    # Get cosine value
    cos_ = dot_prod/magA/magB
    # Get angle in radians and then convert to degrees
    angle = math.acos(dot_prod/magB/magA)
    # Basically doing angle <- angle mod 360
    ang_deg = math.degrees(angle)%360

    if ang_deg-180>=0:
        # As in if statement
        return 360 - ang_deg
    else: 

        return ang_deg"""