import numpy as np
from hexbug_plotter import *
from hexbug import *
from hexbug_world import *
from hexbug_utility import *
import matplotlib.pyplot as plt
from math import *



#x_init = np.random.rand(400)+100
#y_init = np.random.rand(400)+100
#o_init = np.random.rand(2*pi)
x_init = 200.0
y_init = 200.0
o_init = 0.0
time_s = 2.0
mes_ps = 60.0
rp_min = 90.0
rp_sec = (90.0/60.0)
rp_measure = (rp_sec/mes_ps)
u_dist = 100.0
d_dist = 50.0
u_turn = (2*pi) / 40.0
d_turn = (2*pi) / 20.0
col_d_c = 0.5
col_a_c = 0.5
col_d_n = 0.5
col_a_n = 0.5
m_o = (pi+(pi/2))
mt = [[],[]]



boundaries = []
boundaries.append(['x', 'lt', 100])
boundaries.append(['x', 'gt', 800])
boundaries.append(['y', 'lt', 100])
boundaries.append(['y', 'gt', 600])
boundaries.append(['circle', 450, 350, 60])


x1 = 400.0
x2 = 430.0

y1 = 450.0
y2 = 250.0

dx = x2 - x1
dy = y2 - y1
#lx_arr = np.arange(x1, x2)


#tly_n = tly_arr/np.linalg.norm(tly_arr)


hb = hexbug(x_init, y_init, o_init, u_turn, d_turn, u_dist, d_dist, col_d_c, col_a_c, m_o)
hbw = hexbug_world(boundaries)
hb.set_hbw(hbw)
#hbw.set_hb(hb)

for i in range(0, (int(time_s*mes_ps))):
    hb.r_move(rp_measure)
    mt[0].append(hb.x)
    mt[1].append(hb.y)

#print(mt)

hbp = hexbug_plotter(boundaries, col_d_c, col_a_c)
#hbp.visualize_simulation(mt, boundaries)
#hbp.visualize_circular_colission(x1, y1, x2, y2, (pi*2*0.77), boundaries[4])
#hbp.visualize_circular_colission(x1, 200, x2, 350, (pi*2*0.22), boundaries[4])

hbp.visualize_circular_colission_2(x1, y1, x2, y2, (pi*2*0.77), 0.5, 1.0, boundaries[4])
hbp.visualize_circular_colission_2(x1, 200, x2, 350, (pi*2*0.22), 0.5, 1.0, boundaries[4])
hbp.visualize_circular_colission_2(600, 450, 500, 350, (pi*2*0.68), 0.5, 1.0, boundaries[4])

hbp.visualize_point_colission(200.0, 300.0, 0.0, 400, (pi*2*0.45), boundaries[0])
hbp.visualize_point_colission(700.0, 400.0, 900.0, 750, (pi*2*0.15), boundaries[1])
hbp.visualize_point_colission(x1, 150.0, x2, 0.0, (pi*2*0.78), boundaries[2])
hbp.visualize_point_colission(x1, 400.0, x2, 700, (pi*2*0.25), boundaries[3])
hbp.show_plots()



# TODO: MODIFY HEADING AS A FUNCTION OF CUR HEADING, MIN (TAN/NORM & INCIDENCE ANGLE DIFF), COL_ANG_COEFF








print("Colission Detected for", x2, y2, " ?")
print(hbw.is_colission(x2, y2))

x2 = 491.9405933986004
y2 = 256.45406282476597

print("Colission Detected for", x2, y2, " ?")
print(hbw.is_colission(x2, y2))

x2 = 1000.0
y2 = 200.0

print("Colission Detected for", x2, y2, " ?")
print(hbw.is_colission(x2, y2))

x2 = 0.0
y2 = 200.0

print("Colission Detected for", x2, y2, " ?")
print(hbw.is_colission(x2, y2))

x2 = 200.0
y2 = 1000.0

print("Colission Detected for", x2, y2, " ?")
print(hbw.is_colission(x2, y2))

x2 = 200.0
y2 = 0.0

print("Colission Detected for", x2, y2, " ?")
print(hbw.is_colission(x2, y2))

x2 = 450.0
y2 = 350.0

print("Colission Detected for", x2, y2, " ?")
print(hbw.is_colission(x2, y2))


"""plt.subplot(321)
plt.scatter(x, y, s=80, c=z, marker=">")

plt.subplot(322)
plt.scatter(x, y, s=80, c=z, marker=(5, 0))

verts = list(zip([-1., 1., 1., -1.], [-1., -1., 1., -1.]))
plt.subplot(323)
plt.scatter(x, y, s=80, c=z, marker=(verts, 0))
# equivalent:
#plt.scatter(x,y,s=80, c=z, marker=None, verts=verts)

plt.subplot(324)
plt.scatter(x, y, s=80, c=z, marker=(5, 1))

plt.subplot(325)
plt.scatter(x, y, s=80, c=z, marker='+')

plt.subplot(326)
plt.scatter(x, y, s=80, c=z, marker=(5, 2))

plt.show()"""