import numpy as np
import numpy.linalg as la
from hexbug_plotter import *
from hexbug import *
from hexbug_world import *
from hexbug_utility import *
import matplotlib.pyplot as plt
from math import *

class hexbug_plotter:

    def __init__(self, boundaries, col_d_c, col_a_c):
        """This function is called when you create a new robot. It sets some of 
        the attributes of the robot, either to their default values or to the values
        specified when it is created."""
        self.col_d_c = col_d_c
        self.col_a_c = col_a_c
        self.boundaries = boundaries
        
    def plot_boundaries(self, ax):
        circ = plt.Circle((self.boundaries[4][1], self.boundaries[4][2]), radius=self.boundaries[4][3], color='g', fill=False)
        ax.add_patch(circ)
        #hbpath_ax.scatter(mt[0], mt[1])
        ax.axvline(x=100.0)
        ax.axvline(x=800.0)
        ax.axhline(y=100.0)
        ax.axhline(y=600.0)
        return ax
        
    def show_plots(self):
        plt.show()
        
    def is_circular_colission(self, x, y, b):
        in_circle = ((x - b[1])**2 + (y - b[2])**2 < b[3]**2)
        colission = False
        if in_circle:
            #print("Circle Colission Coords")
            #print(x, y)
            colission = True
        return colission
    def visualize_circular_colission_2(self, x1, y1, x2, y2, heading, col_d_c, col_a_c, b):
        
        ccol_fig=plt.figure("Tangent Colissions")
        plt.axis([0,900,0,700])
        ccol_ax=ccol_fig.add_subplot(1,1,1)
        ccol_ax = self.plot_boundaries(ccol_ax)
        
        
        interval = ((x2-x1)*0.1)
        if(x2 - x1 > 0):
            lx_arr = np.arange(x1, x2, interval)
        else:
            lx_arr = np.arange(x1, x2, interval)
        
        
        """interval = ((x2-x1)*0.02
        if(x2 - x1 > 0):
            lx_arr = np.arange(x1, x2, interval)
        else:
            lx_arr = np.arange(x1, x2, interval)"""
            
        lx_arr = np.append(lx_arr, x2)
        m_i, q_i = calc_line_params(x1, y1, x2, y2)
        ly_arr = m_i * lx_arr + q_i
        
        col_x = 0
        col_y = 0
        for idx, val in enumerate(lx_arr):
            if(self.is_circular_colission(val, ly_arr[idx], b)):
                col_x = val
                col_y = ly_arr[idx]
                break
            ccol_ax.scatter(val, ly_arr[idx], color='blue')
        
        
        ivec = np.asarray([x1-x2, y1-y2])
        
        
        perp_x_arr = np.arange(col_x - b[3], col_x + b[3])
        
        m_p, q_p = calc_line_params(col_x, col_y, b[1], b[2])
        perp_y_arr = m_p * perp_x_arr + q_p
        
        perp_vec = np.asarray([perp_x_arr[-1] - perp_x_arr[0],perp_y_arr[-1] - perp_y_arr[0]])
        
        rvec = reflect(ivec, normalize(perp_vec))
        v = [col_x, col_y], [(col_x-rvec[0]*col_d_c), (col_y-rvec[1]*col_d_c)]
        
        v_i = [[x1, y1], [col_x, col_y]]
       
        #ang = np.arctan2([y1, ])
       
        p_h = [col_x+cos(heading), col_y+sin(heading)]
        p_p1 = [col_x+1, perp_y_arr[(np.where(perp_x_arr==(col_x+1)))]]
        p_p2 = [col_x-1, perp_y_arr[(np.where(perp_x_arr==(col_x-1)))]]
        #p_n = [idx_avg_x+cos(heading), idx_avg_y]
        
        d1 = np.asarray(p_h)-np.asarray(v_i[1]);
        dp1 = np.asarray(p_p1) - np.asarray(v_i[1]);
        dp2 = np.asarray(p_p2) - np.asarray(v_i[1]);
       
        angle_h = atan2(d1[1], d1[0])
        angle_p_1 = atan2(dp1[1], dp1[0])
        angle_p_2 = atan2(dp2[1], dp2[0])
        
        #a = min(angle_t_1-angle_h, angle_t_2-angle_h)
        """if(abs(angle_p_1-angle_h) < abs(angle_p_2-angle_h)):
            a = angle_p_1-angle_h
        else:
            a = angle_p_2-angle_h"""
            
        if(abs(angle_p_1-angle_h) < abs(angle_p_2-angle_h)):
            a = angle_h-angle_p_1
        else:
            a = angle_h-angle_p_2
       
        #a = angle2-angle_h;
        
        if(abs(a) > (pi/4)):
            if(a > 0):
                a = (pi/2) - a
            else:
                a = (-pi/2) - a
        a = (a * col_a_c)
        
        #ccol_ax.plot(lx_arr_2, ly_arr)
        ccol_ax.plot([x1,col_x], [y1, col_y], 'r')
        ccol_ax.plot(perp_x_arr, perp_y_arr, 'g--')
        ccol_ax.scatter(col_x, col_y, color='red')
        ccol_ax.scatter((col_x-rvec[0]*self.col_d_c), (col_y-rvec[1]*self.col_d_c), color='green')
        #ccol_ax.plot([idx_avg_x, (idx_avg_x+rvec[0])], [idx_avg_y, (idx_avg_y+rvec[1])], 'b--')
        ccol_ax.plot([col_x, (col_x-rvec[0])], [col_y, (col_y-rvec[1])], 'b--')
        ccol_ax.plot([col_x, (col_x-rvec[0]*self.col_d_c)], [col_y, (col_y-rvec[1]*self.col_d_c)], 'r')
        
        #ccol_ax.arrow(x1, y1, (cos(heading)*10), (sin(heading)*10), head_width=10.0, head_length=10.0, fc='k', ec='k')
        #ccol_ax.arrow((col_x-rvec[0]*self.col_d_c), (col_y-rvec[1]*self.col_d_c), (cos(heading+a)*10), (sin(heading+a)*10), head_width=10.0, head_length=10.0, fc='k', ec='k')
        
        
    def visualize_circular_colission(self, x1, y1, x2, y2, heading, b):   
        dx = x2 - x1
        dy = y2 - y1
        lx_arr = np.arange(0, 1000)
        lx_arr_2 = np.arange(0 - b[3], 0 + b[3], 0.1)
        lx_arr_rv = np.arange(0 - b[3]*2, 0 + b[3]*2, 0.1)
        semi_c_top = np.sqrt(b[3]**2 - lx_arr_2**2)
        semi_c_bottom = semi_c_top * -1.0
        semi_c_top = semi_c_top + b[2]
        semi_c_bottom = semi_c_bottom + b[2]
        lx_arr_2 = lx_arr_2 + b[1]
        lx_arr_rv = lx_arr_rv + b[1]
        
        m = 0.0
        if(dx == 0):
            m = 999999
        else:
            m = ((dy)/(dx))
        q = y2-m*x2
        ly_arr = m * lx_arr_2 + q
        
        
        idx = []
        idx_avg_x = 0.0
        idx_avg_y = 0.0
        semi_c_y = []
        idx_t = (np.argwhere(np.isclose(semi_c_top, ly_arr, atol=1)).reshape(-1))
        idx_b = (np.argwhere(np.isclose(semi_c_bottom, ly_arr, atol=1)).reshape(-1))
        
                
        if(idx_t.size == 0):
            idx = idx_b
            idx_avg_x_b = int(np.average(lx_arr_2[idx_b]))
            idx_avg_y_b = int(np.average(semi_c_bottom[idx_b]))
            idx_avg_x = idx_avg_x_b
            idx_avg_y = idx_avg_y_b
            semi_c_y = semi_c_bottom
        elif(idx_b.size == 0):
            idx = idx_t
            idx_avg_x_t = int(np.average(lx_arr_2[idx_t]))
            idx_avg_y_t = int(np.average(semi_c_top[idx_t]))
            idx_avg_x = idx_avg_x_t
            idx_avg_y = idx_avg_y_t
            semi_c_y = semi_c_top
        else:
            idx_avg_x_t = int(np.average(lx_arr_2[idx_t]))
            idx_avg_y_t = int(np.average(semi_c_top[idx_t]))
            idx_avg_x_b = int(np.average(lx_arr_2[idx_b]))
            idx_avg_y_b = int(np.average(semi_c_bottom[idx_b]))
            dist_t = distance_between([idx_avg_x_t, idx_avg_y_t], [x1, y1])
            dist_b = distance_between([idx_avg_x_b, idx_avg_y_b], [x1, y1])
        
            if(dist_t < dist_b):
                idx = idx_t
                idx_avg_x = idx_avg_x_t
                idx_avg_y = idx_avg_y_t
                semi_c_y = semi_c_top
            else:
                idx = idx_b
                idx_avg_x = idx_avg_x_b
                idx_avg_y = idx_avg_y_b
                semi_c_y = semi_c_bottom
        
        
        #idx_avg_x = int(np.average(lx_arr_2[idx]))
        #idx_avg_y = int(np.average(semi_c_bottom[idx]))
        
        ivec = [x1-x2, y1-y2]
        
        
        
        #x = np.array([-1, +1, +1, -1])
        #y = np.array([-1, -1, +1, +1])
        #np.arctan2(y, x) * 180 / np.pi
        
        #ly_arr_relfection = 
        
        
        lx_arr_perp = np.arange(idx_avg_x - 50, idx_avg_x + 50)
        tan_params = np.polyfit(lx_arr_2[idx], semi_c_y[idx], 1)
        
        tly_arr = tan_params[0] * lx_arr_perp + tan_params[1]
        b = idx_avg_y-(idx_avg_x*(-1/tan_params[0]))
        tly_perp_arr = (-1/tan_params[0]) * lx_arr_perp + b
        
        tvec = [lx_arr_perp[-1] - lx_arr_perp[0],tly_arr[-1] - tly_arr[0]]
        norm = perpendicular(normalize(tvec))
        rvec = reflect(ivec, norm)
        
        
        v_i = [[x1, y1], [idx_avg_x, idx_avg_y]]
       
        #ang = np.arctan2([y1, ])
       
        p_h = [idx_avg_x+cos(heading), idx_avg_y+sin(heading)]
        p_t1 = [idx_avg_x+1, tly_arr[(np.where(lx_arr_perp==(idx_avg_x+1)))]]
        p_t2 = [idx_avg_x-1, tly_arr[(np.where(lx_arr_perp==(idx_avg_x-1)))]]
        #p_n = [idx_avg_x+cos(heading), idx_avg_y]
        
        d1 = np.asarray(p_h)-np.asarray(v_i[1]);
        dt1 = np.asarray(p_t1) - np.asarray(v_i[1]);
        dt2 = np.asarray(p_t2) - np.asarray(v_i[1]);
       
        angle_h = atan2(d1[1], d1[0])
        angle_t_1 = atan2(dt1[1], dt1[0])
        angle_t_2 = atan2(dt2[1], dt2[0])
        
        #a = min(angle_t_1-angle_h, angle_t_2-angle_h)
        if(abs(angle_t_1-angle_h) < abs(angle_t_2-angle_h)):
            a = angle_t_1-angle_h
        else:
            a = angle_t_2-angle_h
       
        #a = angle2-angle_h;
        
        if(abs(a) > (pi/4)):
            if(a > 0):
                a = (pi/2) - a
            else:
                a = (-pi/2) - a
        #heading_p = (heading + a)
        
        print("Circular Colission Prior / Post heading, Delta")   
        print(heading, heading+a, a)
        print("Circular Colission Tan Deltas")   
        print(angle_h, angle_t_1, angle_t_2, (angle_t_1 - angle_h), (angle_t_2 - angle_h))
        
        
        ccol_fig=plt.figure("Tangent Colissions")
        plt.axis([0,600,0,400])
        ccol_ax=ccol_fig.add_subplot(1,1,1)
        ccol_ax.scatter(lx_arr_2, semi_c_top, color='g')
        ccol_ax.scatter(lx_arr_2, semi_c_bottom, color='g')
        ccol_ax.scatter(lx_arr_2[idx], semi_c_y[idx], color='r')
        #ccol_ax.plot(lx_arr_2, ly_arr)
        ccol_ax.plot([x1,idx_avg_x], [y1, idx_avg_y], 'r')
        ccol_ax.plot(lx_arr_perp, tly_arr, 'r--')
        ccol_ax.plot(lx_arr_perp, tly_perp_arr, 'g--')
        #ccol_ax.plot([idx_avg_x, (idx_avg_x+rvec[0])], [idx_avg_y, (idx_avg_y+rvec[1])], 'b--')
        ccol_ax.plot([idx_avg_x, (idx_avg_x-rvec[0])], [idx_avg_y, (idx_avg_y-rvec[1])], 'b--')
        ccol_ax.plot([idx_avg_x, (idx_avg_x-rvec[0]*self.col_d_c)], [idx_avg_y, (idx_avg_y-rvec[1]*self.col_d_c)], 'r')
        
        ccol_ax.arrow(x1, y1, (cos(heading)*10), (sin(heading)*10), head_width=10.0, head_length=10.0, fc='k', ec='k')
        ccol_ax.arrow((idx_avg_x-rvec[0]*self.col_d_c), (idx_avg_y-rvec[1]*self.col_d_c), (cos(heading+a)*10), (sin(heading+a)*10), head_width=10.0, head_length=10.0, fc='k', ec='k')
        
    def visualize_point_colission(self, x1, y1, x2, y2, heading, b):
        #v, a = eval_point_colission(x1, y1, x2, y2, heading, 0.5, 1.0, b)
        dx = x2 - x1
        dy = y2 - y1
        #lx_arr = np.arange(x1, x2)
        
        intercept_x = 0.0
        intercept_y = 0.0
        
        vx_arr = np.arange(x1, x2)
        m = ((dy)/(dx))
        q = y2-m*x2
        vy_arr = m * vx_arr + q
        p_i = []
        p_b = []
        p_r = []
        p_h = []
        p_t = []
        
        if b[0] == 'x':
            intercept_x = b[2]
            intercept_y = m * intercept_x + q
            dx = x2 - intercept_x
            dy = y2 - intercept_y
            
            p_b = [intercept_x - dx, intercept_y+dy]
            p_r = [intercept_x - (dx*self.col_d_c), intercept_y+(dy*self.col_d_c)]
            p_h = [intercept_x+cos(heading), intercept_y+sin(heading)]
            p_t = [intercept_x, intercept_y+sin(heading)]
                    
        elif b[0] == 'y':
            intercept_y = b[2]
            intercept_x = (intercept_y - q) / m
            dx = x2 - intercept_x
            dy = y2 - intercept_y
            p_b = [intercept_x+dx, intercept_y-dy]
            p_r = [intercept_x+(dx*self.col_d_c), intercept_y-(dy*self.col_d_c)]
            p_h = [intercept_x+cos(heading), intercept_y+sin(heading)]
            p_t = [intercept_x+cos(heading), intercept_y]
        
   
        p_i = [[x1, y1], [intercept_x, intercept_y]]
       
        #ang = np.arctan2([y1, ])
       
        d1 = np.asarray(p_h)-np.asarray(p_i[1]);
        d2 = np.asarray(p_t) - np.asarray(p_i[1]);
       
        angle1 = atan2(d1[1], d1[0])
        angle2 = atan2(d2[1], d2[0])
       
        a = angle2-angle1;
        if(abs(a) > (pi/4)):
            if(a > 0):
                a = (pi/2) - a
            else:
                a = (-pi/2) - a
        print("angle & border")
        print(a)
        print(b)
        
        
        
        
        
        
        """THIS IS IMPORTANT"""        
        """Calc angle diff between heading x1,y1 -> cos(heading), sin(heading) AND x1,y1 -> inf/0/x1, inf/0/y1"""
        """COMPARE ANG NORM AND ANG TAN, USE SMALLEST"""
        
        

        #cosang_v = np.dot(p_i, [[0,0],[0,1]])
        #sinang_v = la.norm(np.cross(p_i, [[0,0],[0,1]]))
        #ang_diff_v = np.arctan2(sinang_v, cosang_v) 
        
        #cosang_h = np.dot(p_i, [[0,0],[1,0]])
        #sinang_h = la.norm(np.cross(p_i, [[0,0],[1,0]]))
        #ang_diff_h = np.arctan2(sinang_h, cosang_h) 
        
        #ang_diff = min(ang_diff_v, ang_diff_h)
        #ang_diff = ang_diff_v
        

        #x_a = (  (p_r[1][0] - intercept_x) * cos(ang_diff) + (p_r[1][1] - intercept_y) * sin(ang_diff) ) + intercept_x
        #y_a = ( -(p_r[1][0] - intercept_x) * sin(ang_diff) + (p_r[1][1] - intercept_y) * cos(ang_diff) ) + intercept_y
        
        pcol_fig=plt.figure("Point Colissions")
        plt.axis([0,900,0,700])
        pcol_ax=pcol_fig.add_subplot(1,1,1)
        pcol_ax = self.plot_boundaries(pcol_ax)
        pcol_ax.plot([x1,intercept_x], [y1, intercept_y], 'g')
        pcol_ax.plot([intercept_x, (p_b[0])], [intercept_y, (p_b[1])], 'b--')
        pcol_ax.plot([intercept_x, (p_r[0])], [intercept_y, (p_r[1])], 'r')
        #pcol_ax.plot([intercept_x, (x_a)], [intercept_y, (y_a)], 'g')
        pcol_ax.arrow(x1, y1, (cos(heading)*10), (sin(heading)*10), head_width=10.0, head_length=10.0, fc='k', ec='k')
        pcol_ax.arrow(p_r[0], p_r[1], (cos(heading+a)*10), (sin(heading+a)*10), head_width=10.0, head_length=10.0, fc='k', ec='k')
    
    
    def visualize_simulation(self, mt, boundaries):
        hbpath_fig=plt.figure()
        plt.axis([0,900,0,700])
        hbpath_ax=hbpath_fig.add_subplot(1,1,1)
        hbpath_ax = self.plot_boundaries(hbpath_ax)
        hbpath_ax.scatter(mt[0], mt[1])